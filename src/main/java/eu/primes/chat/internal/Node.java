package eu.primes.chat.internal;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This allows to create nodes and access node data.
 * 
 * @author Ivan Goenawan
 */

public class Node {
	private String uniqueId;
	private String alternativeId;
	private String aliases;
	private String ncbiTaxId;
	
	private ArrayList<String> idList;  //only contains IDs of the selected ID type, all in lower case
	private String displayName;
	
	private String suppliedId;	
	private String contextAttribute;
	private boolean contextual;
	private int numNeighbours;
	private int numContextualNeighbours;
	private double pValue;
	private double adjustedPValue;
	
	//this is for identifying human-readable / display names of nodes
	//the matcher has to be made non static if we're going to make the program multi-threaded
	private static final Pattern hasIdDescription = Pattern.compile("\\([^()]+\\)$");
	private static final Matcher hasIdDescriptionMatcher = hasIdDescription.matcher("");
	
	public Node(String uniqueId, String alternativeId, String aliases, String ncbiTaxId, String chosenIdType){
		this.uniqueId = uniqueId;
		this.alternativeId = alternativeId;
		this.aliases = aliases;
		this.ncbiTaxId = ncbiTaxId;
		
		displayName = "";
		idList = new ArrayList<String>();
		String allIds = uniqueId + "|" + alternativeId + "|" + aliases;
		String[] ids = allIds.split("\\|");
		for (int i = 0; i < ids.length; i++){
			
			if (ids[i].indexOf(":") != -1){
				String[] tokens = ids[i].split(":", 2);
				String idType = tokens[0].trim();
				String id = tokens[1].trim();
				
				if (chosenIdType.equalsIgnoreCase(idType)){
					idList.add(id.toLowerCase());
				}
				
				if (hasIdDescriptionMatcher.reset(id).find()){
					id = id.substring(0, hasIdDescriptionMatcher.start());
					if (displayName.isEmpty() || id.length() < displayName.length()){
						displayName = id;
					}
				}
			
			}
		
		}
		
		
		suppliedId = "";
		contextual = false;
		numNeighbours = 0;
		numContextualNeighbours = 0;
	}
	
	public void incrementNeighbour(boolean contextual){
		numNeighbours++;
		if (contextual) numContextualNeighbours++;
	}
	

	/**
	 * getters and setters
	 */

	public String getUniqueId() {
		return uniqueId;
	}

	public String getAlternativeId() {
		return alternativeId;
	}

	public String getAliases() {
		return aliases;
	}

	public String getNCBITaxId() {
		return ncbiTaxId;
	}
	
	
	public String getSuppliedId() {
		return suppliedId;
	}


	public void setSuppliedId(String customName) {
		this.suppliedId = customName;
	}


	public int getNumNeighbours() {
		return numNeighbours;
	}


	public void setNumNeighbours(int numNeighbours) {
		this.numNeighbours = numNeighbours;
	}


	public int getNumContextualNeighbours() {
		return numContextualNeighbours;
	}


	public void setNumContextualNeighbours(int numContextualNeighbours) {
		this.numContextualNeighbours = numContextualNeighbours;
	}
	
	public String getContextAttribute() {
		return contextAttribute;
	}


	public void setContextAttribute(String contextAttribute) {
		this.contextAttribute = contextAttribute;
	}


	public boolean isContextual() {
		return contextual;
	}


	public void setContextual(boolean contextual) {
		this.contextual = contextual;
	}
	
	
	//return IDs of the chosen ID type
	public ArrayList<String> getIdList(){
		return idList;
	}
	
	public String getDisplayName(){
		return displayName;
	}
	
	public void setPValue(double pValue){
		this.pValue = pValue; 
	}
	
	public double getPValue(){
		return pValue;
	}
	
	public void setAdjustedPValue(double adjustedPValue){
		this.adjustedPValue = adjustedPValue;
	}
	
	public double getAdjustedPValue(){
		return adjustedPValue;
	}
}

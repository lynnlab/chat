package eu.primes.chat.internal;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.DefaultListSelectionModel;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.JTableHeader;

import org.cytoscape.app.CyAppAdapter;
import org.cytoscape.model.CyEdge.Type;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyNode;
import org.cytoscape.model.CyRow;
import org.cytoscape.util.swing.BasicCollapsiblePanel;


/**
 * This creates the node analyzer view in the results panel.
 * 
 * @author Tanja Muetze, Ivan Goenawan
 */

public class NodeAnalyzerPanel extends BasicCollapsiblePanel{

	private JTable neighbourTable;
	private JLabel nodeNameValueLabel;
	private JLabel pValueNumberLabel;
	private JButton exitNodeAnalyzerButton;
	
	private List<CyNode> neighbours;

	private CyNetwork network;

	protected Object appAdapter;
	
	public NodeAnalyzerPanel(final CyAppAdapter appAdapter, CyNetwork network, final ResultPanel resultPanel){
		super("Node Analyzer");
		
		this.network = network;
		this.neighbours = new ArrayList<CyNode>();
		
		nodeNameValueLabel = new JLabel("-");
		pValueNumberLabel = new JLabel("-");
		exitNodeAnalyzerButton = new JButton("Exit Node Analyzer");
		
		
		exitNodeAnalyzerButton.setEnabled(false);
		exitNodeAnalyzerButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				/**
				 * Button to exit the node analyzer view.
				 */
				nodeNameValueLabel.setText("-");
				pValueNumberLabel.setText("-");
				
				neighbours = new ArrayList<CyNode>();
				((AbstractTableModel)neighbourTable.getModel()).fireTableDataChanged();
				
				exitNodeAnalyzerButton.setEnabled(false);
				
				
				resultPanel.nodeAnalyzerExited();
			}

		});
		
		
		
		neighbourTable = new JTable(){
			@Override
			protected void paintComponent(Graphics g) {
			    super.paintComponent(g);
			    if(getRowCount() == 0) {
			    	Graphics2D g2d = (Graphics2D) g;
			    	g2d.setColor(Color.BLACK);
			    	g2d.drawString("Node analyzer not active.", 110, 15);
			    }
			}
		};
		neighbourTable.setFillsViewportHeight(true);
		neighbourTable.setSelectionMode(DefaultListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		neighbourTable.setCellSelectionEnabled(true);
		
		//this is to allow copying data from the table
		neighbourTable.registerKeyboardAction(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
		        JTable table = (JTable)e.getSource();
				int[] rowsSelected=table.getSelectedRows();
		        int[] colsSelected=table.getSelectedColumns();
		        
		        StringBuffer sbf=new StringBuffer();
		        
		        for (int i = 0; i < rowsSelected.length; i++){
		        	for (int j = 0; j < colsSelected.length; j++){
		        		sbf.append(table.getValueAt(rowsSelected[i],colsSelected[j]));
		        		if (j < colsSelected.length - 1) sbf.append("\t");
		            }
		            sbf.append("\n");
		        }
		        StringSelection stsel  = new StringSelection(sbf.toString());
		        Clipboard systemClipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
		        systemClipboard.setContents(stsel,stsel);
			}
		}, KeyStroke.getKeyStroke(KeyEvent.VK_C,ActionEvent.CTRL_MASK,false), JComponent.WHEN_FOCUSED);
		
		neighbourTable.setModel(new AbstractTableModel() {
			
			@Override
			public Object getValueAt(int rowIndex, int columnIndex) {
				CyNode node = neighbours.get(rowIndex);
				CyRow row = NodeAnalyzerPanel.this.network.getRow(node);
				
				switch(columnIndex){
					case 0:
						String displayName = row.get(ConstructNetworkTask.NODE_DISPLAY_NAME, String.class);
						if (displayName.isEmpty()) displayName = row.get(CyNetwork.NAME, String.class);
						return displayName;
					case 1:
						return row.get(ConstructNetworkTask.NODE_CONTEXT_ATTRIBUTE, Object.class);
					case 2:
						return row.get(ConstructNetworkTask.NODE_CONTEXTUALLY_IMPORTANT, Object.class);
					default:
						return null;
				}
			}
			
			@Override
			public int getRowCount() {
				return neighbours.size();
			}
			
			@Override
			public int getColumnCount() {
				return 3;
			}
			
			@Override
			public Class<?> getColumnClass(int columnIndex) {
				switch (columnIndex){
					case 0:
						return String.class;
					case 1: 
						return NodeAnalyzerPanel.this.network.getDefaultNodeTable().getColumn(ConstructNetworkTask.NODE_CONTEXT_ATTRIBUTE).getType();
					case 2:
						return Boolean.class;
					default:
						return null;	
				}
			}

			@Override
			public String getColumnName(int column) {
				switch (column){
					case 0:
						return "Name";
					case 1:
						return "Context Attribute";
					case 2:
						return "Contextual";
					default:
						return null;
				}
			}
		});
		neighbourTable.setAutoCreateRowSorter(true);
		neighbourTable.getRowSorter().toggleSortOrder(2);
		neighbourTable.getRowSorter().toggleSortOrder(2);
		
		
		
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{10, 150, 20, 200, 10, 0};
		gridBagLayout.rowHeights = new int[]{10, 0, 0, 30, 10, 120};
		gridBagLayout.columnWeights = new double[]{0.0, 0.0, 0.0, 1.0, 0.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		getContentPane().setLayout(gridBagLayout);


		JLabel nodeLabel = new JLabel("Node:");
		GridBagConstraints gbc_nodeLabel = new GridBagConstraints();
		gbc_nodeLabel.anchor = GridBagConstraints.NORTHWEST;
		gbc_nodeLabel.insets = new Insets(0, 0, 5, 5);
		gbc_nodeLabel.gridx = 1;
		gbc_nodeLabel.gridy = 1;
		add(nodeLabel, gbc_nodeLabel);
		
		
		GridBagConstraints gbc_nodeNameValueLabel = new GridBagConstraints();
		gbc_nodeNameValueLabel.anchor = GridBagConstraints.NORTHWEST;
		gbc_nodeNameValueLabel.insets = new Insets(0, 0, 5, 5);
		gbc_nodeNameValueLabel.gridx = 2;
		gbc_nodeNameValueLabel.gridy = 1;
		add(nodeNameValueLabel, gbc_nodeNameValueLabel);
		
		
		GridBagConstraints gbc_exitNodeAnalyzerButton = new GridBagConstraints();
		gbc_exitNodeAnalyzerButton.anchor = GridBagConstraints.NORTHEAST;
		gbc_exitNodeAnalyzerButton.insets = new Insets(0, 0, 5, 5);
		gbc_exitNodeAnalyzerButton.gridx = 3;
		gbc_exitNodeAnalyzerButton.gridy = 1;
		add(exitNodeAnalyzerButton, gbc_exitNodeAnalyzerButton);


		JLabel pValueLabel = new JLabel("P-value:");
		GridBagConstraints gbc_pValueLabel = new GridBagConstraints();
		gbc_pValueLabel.anchor = GridBagConstraints.NORTHWEST;
		gbc_pValueLabel.insets = new Insets(0, 0, 5, 5);
		gbc_pValueLabel.gridx = 1;
		gbc_pValueLabel.gridy = 2;
		add(pValueLabel, gbc_pValueLabel);
		
		
		GridBagConstraints gbc_pValueNumberLabel = new GridBagConstraints();
		gbc_pValueNumberLabel.anchor = GridBagConstraints.NORTHWEST;
		gbc_pValueNumberLabel.insets = new Insets(0, 0, 5, 5);
		gbc_pValueNumberLabel.gridx = 2;
		gbc_pValueNumberLabel.gridy = 2;
		add(pValueNumberLabel, gbc_pValueNumberLabel);


		JLabel directNeighboursLabel = new JLabel("First neighbors:");
		GridBagConstraints gbc_directNeighboursLabel = new GridBagConstraints();
		gbc_directNeighboursLabel.gridwidth = 3;
		gbc_directNeighboursLabel.anchor = GridBagConstraints.NORTHWEST;
		gbc_directNeighboursLabel.insets = new Insets(0, 0, 5, 5);
		gbc_directNeighboursLabel.gridx = 1;
		gbc_directNeighboursLabel.gridy = 3;
		add(directNeighboursLabel, gbc_directNeighboursLabel);

		
		GridBagConstraints gbc_neighbourTable = new GridBagConstraints();
		gbc_neighbourTable.fill = GridBagConstraints.BOTH;
		gbc_neighbourTable.gridwidth = 3;
		gbc_neighbourTable.insets = new Insets(0, 0, 0, 0);
		gbc_neighbourTable.gridx = 1;
		gbc_neighbourTable.gridy = 5;
		add(neighbourTable, gbc_neighbourTable);


		JTableHeader neighbourTableHeader = neighbourTable.getTableHeader();
		GridBagConstraints gbc_neighbourTableHeader = new GridBagConstraints();
		gbc_neighbourTableHeader.gridwidth = 3;
		gbc_neighbourTableHeader.insets = new Insets(0, 0, 0, 0);
		gbc_neighbourTableHeader.fill = GridBagConstraints.BOTH;
		gbc_neighbourTableHeader.gridx = 1;
		gbc_neighbourTableHeader.gridy = 4;
		add(neighbourTableHeader, gbc_neighbourTableHeader);
	
	}
	
	public void nodeAnalyzerActivated(CyNode node){
		String displayName = network.getRow(node).get(ConstructNetworkTask.NODE_DISPLAY_NAME, String.class);
		if (displayName.isEmpty())displayName = network.getRow(node).get(CyNetwork.NAME, String.class);
		
		nodeNameValueLabel.setText(displayName);
		pValueNumberLabel.setText(network.getRow(node).get(ConstructNetworkTask.NODE_ADJUSTED_PVALUE, Double.class).toString());
		
		neighbours = network.getNeighborList(node, Type.ANY);
		((AbstractTableModel)neighbourTable.getModel()).fireTableDataChanged();
		
		exitNodeAnalyzerButton.setEnabled(true);
	}

}

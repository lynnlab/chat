package eu.primes.chat.internal;

/**
 * This accesses the Mitab file containing the interaction data downloaded from one of the
 * PSICQUIC services.
 * 
 * @author Ivan Goenawan
 */

public class MitabEntry {
	private String uniqueIdA;
	private String alternativeIdA;
	private String aliasesA;
	private String ncbiTaxIdA;
	
	private String uniqueIdB;
	private String alternativeIdB;
	private String aliasesB;
	private String ncbiTaxIdB;
	
	private String detectionMethod;
	private String firstAuthor;
	private String publicationId;
	private String interactionType;
	private String sourceDatabases;
	private String interactionId;
	private String confidenceScore;
	
	
	public MitabEntry(String mitabLine){
		String[] fields = mitabLine.split("\t");
		
		this.uniqueIdA = fields[0].trim();
		this.alternativeIdA = fields[2].trim();
		this.aliasesA = fields[4].trim();
		this.ncbiTaxIdA = fields[9].trim();
		
		this.uniqueIdB = fields[1].trim();
		this.alternativeIdB = fields[3].trim();
		this.aliasesB = fields[5].trim();
		this.ncbiTaxIdB = fields[10].trim();
		
		this.detectionMethod = fields[6].trim();
		this.firstAuthor = fields[7].trim();
		this.publicationId = fields[8].trim();
		this.interactionType = fields[11].trim();
		this.sourceDatabases = fields[12].trim();
		this.interactionId = fields[13].trim();
		this.confidenceScore = fields[14].trim();
	}

	public String getUniqueIdA() {
		return uniqueIdA;
	}

	public String getAlternativeIdA() {
		return alternativeIdA;
	}

	public String getAliasesA() {
		return aliasesA;
	}

	public String getNcbiTaxIdA() {
		return ncbiTaxIdA;
	}

	public String getUniqueIdB() {
		return uniqueIdB;
	}
	
	public String getAlternativeIdB() {
		return alternativeIdB;
	}

	public String getAliasesB() {
		return aliasesB;
	}

	public String getNcbiTaxIdB() {
		return ncbiTaxIdB;
	}

	public String getDetectionMethod() {
		return detectionMethod;
	}

	public String getFirstAuthor() {
		return firstAuthor;
	}

	public String getPublicationId() {
		return publicationId;
	}

	public String getInteractionType() {
		return interactionType;
	}
	
	public String getInteractionTypeId(){
		if (interactionType.startsWith("psi-mi:\"MI:")){
			return interactionType.substring(11,15);
		}else if (interactionType.equals("-")){
			return interactionType;
		}else if (interactionType.startsWith("psimi:\"MI:")){        //only I2D uses this format
			return interactionType.substring(10,14);
		}else if (interactionType.startsWith("Mi:0933")){            //bug with Spike
			return "0933";
		}else if (interactionType.startsWith("intact:\"IA:2752")){   //intact sulfurtransfer reaction
			return "1327";                                //convert to psi-mi
		}else{
			System.out.println("unrecognized interaction type: " + interactionType);
			return null;
		}
	}

	public String getSourceDatabases() {
		return sourceDatabases;
	}

	public String getInteractionId() {
		return interactionId;
	}
	
	public String getConfidenceScore() {
		return confidenceScore;
	}
}

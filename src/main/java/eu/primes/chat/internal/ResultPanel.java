package eu.primes.chat.internal;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Paint;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EventObject;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.swing.DefaultListCellRenderer;
import javax.swing.DefaultListModel;
import javax.swing.GroupLayout;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.UIManager;
import javax.swing.border.MatteBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;

import org.cytoscape.app.CyAppAdapter;
import org.cytoscape.application.swing.CytoPanelComponent;
import org.cytoscape.application.swing.CytoPanelName;
import org.cytoscape.model.CyColumn;
import org.cytoscape.model.CyEdge;
import org.cytoscape.model.CyEdge.Type;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyNode;
import org.cytoscape.model.CyTable;
import org.cytoscape.util.swing.CyColorChooser;
import org.cytoscape.view.layout.CyLayoutAlgorithm;
import org.cytoscape.view.model.CyNetworkView;
import org.cytoscape.view.model.View;
import org.cytoscape.view.presentation.property.BasicVisualLexicon;
import org.cytoscape.view.vizmap.VisualMappingFunctionFactory;
import org.cytoscape.view.vizmap.VisualStyle;
import org.cytoscape.view.vizmap.mappings.BoundaryRangeValues;
import org.cytoscape.view.vizmap.mappings.ContinuousMapping;
import org.cytoscape.view.vizmap.mappings.DiscreteMapping;
import org.cytoscape.work.TaskIterator;


/**
 * Info panel that pops up when the network is created. Provides a legend to the network, lets
 * the user highlight contextually important nodes, switch between sizing nodes by degree or
 * contextually important neighbors and limit the network size. Allows for color customization.
 * Brings up a table of neighbors if the Node Analyzer is activated on a node. 
 * 
 * @author Tanja Muetze, Ivan Goenawan
 */

public class ResultPanel extends JScrollPane implements CytoPanelComponent, ActionListener{
	private CyAppAdapter appAdapter;
	private CyNetwork network;
	
	private JPanel viewPortPanel;
	private NodeAnalyzerPanel nodeAnalyzerPanel;
	private JLabel legendLabel;
	private JLabel speciesLabel;
	private JLabel databaseLabel;
	private JLabel idTypeLabel;
	private JLabel interactionTypeLabel;
	private JList<String> interactionList;
	private JScrollPane interactionTypeListScrollPane;
	private JCheckBox highlightContextualNodesButton;
	private JToggleButton degreeButton;
	private JToggleButton contextualNeighborsButton;
	private JLabel sliderInstructionLabel;
	private JSlider slider;
	private JTextField sliderValueField;
	private JLabel errorLabelForSliderValue;
	private JComboBox<CyLayoutAlgorithm> layoutComboBox;
	private JButton reLayoutButton;

	private JPanel dbAndSpeciesPanel;
	private JPanel modifyNetworkDesignPanel;
	private JPanel sliderPanel;
	private JPanel colorSchemaBorderPanel;
	private JPanel nodeAnalyzerBorderPanel;
	
	
	private VisualMappingFunctionFactory discreteMappingFactory;
	private VisualMappingFunctionFactory continuousMappingFactory;
	private VisualStyle visualStyle;
	
	private int maxNumNeighbor;
	private double maxLogPVal;
	private int maxNumberNodesShown;
	
	private ArrayList<SortableNodeObject> pValueNodeRank;
	private ArrayList<SortableNodeObjectInt> neighborsNodeRank; 
	private Boolean UPDATE;
	private int DEFAULTNUMBEROFNODESSHOWN;
	
	private String networkName;
	
	private String[] userDefinedParameters;
	private ArrayList<String> interactionTypesNamesList;
	
	private Color lowerColorShade;
	private Color upperColorShade;
	private Color contextualColor;
	private Color notContextualColor;
	
	public ResultPanel(final CyAppAdapter appAdapter, final CyNetwork network, String networkName, String[] userDefinedParameters, ArrayList<String> interactionTypesNamesList) {

		this.appAdapter = appAdapter;
		this.network = network;
		this.viewPortPanel = new JPanel();
		this.networkName = networkName;
		this.userDefinedParameters = userDefinedParameters;
		this.interactionTypesNamesList = interactionTypesNamesList;
		
		lowerColorShade = Color.WHITE;
		upperColorShade = new Color(188, 6, 55);
		contextualColor = new Color(0x06B871);
		notContextualColor = Color.LIGHT_GRAY;
		
		DEFAULTNUMBEROFNODESSHOWN = 20;
		maxNumberNodesShown = DEFAULTNUMBEROFNODESSHOWN;

		
		pValueNodeRank = computeNodeRankingBasedOnPValue();
		hideAllExceptTopXNodesAndNeighbors(pValueNodeRank, DEFAULTNUMBEROFNODESSHOWN);
		
		/**
		 * panel to displayed user-supplied information about the database and species
		 * that were used to create the network
		 */
		
		dbAndSpeciesPanel = new JPanel();
		dbAndSpeciesPanel.setBorder(new TitledBorder("User-supplied settings"));
		
		databaseLabel = new JLabel("Database: " + userDefinedParameters[0]);
		speciesLabel = new JLabel("Species: " + userDefinedParameters[1]);
		idTypeLabel = new JLabel("ID type: "+ userDefinedParameters[2]);
		
		interactionTypeLabel = new JLabel("Interaction type(s):");
		DefaultListModel<String> interactionTypesListModel = new DefaultListModel<String>();
		for (String interactionTypeName: interactionTypesNamesList){
			interactionTypesListModel.addElement(interactionTypeName);
		}
		interactionList = new JList<String>(){
			@Override
		    public Dimension getPreferredScrollableViewportSize() {
		        Dimension size = super.getPreferredScrollableViewportSize();
		        size.height = 35;
		        return size;
		    }
		};
		interactionList.setModel(interactionTypesListModel);
		interactionList.setCellRenderer(new DefaultListCellRenderer());
		
		/**
		 * panel to manipulate (the design of) the network: highlighting contextual or degree hubs
		 * and coloring contextual nodes.
		 */
		
		modifyNetworkDesignPanel = new JPanel();
		modifyNetworkDesignPanel.setBorder(new TitledBorder("Compare contextual with degree-based hubs"));
		
		contextualNeighborsButton = new JToggleButton("Show contextual hubs");
		degreeButton = new JToggleButton("Show degree hubs");
		highlightContextualNodesButton = new JCheckBox("Show contextually important nodes");
		
		maxNumNeighbor = 0;
		CyColumn numNeighbourColumn = network.getDefaultNodeTable().getColumn(ConstructNetworkTask.NODE_TOTAL_NEIGHBOURS);
		for (Integer numNeighbour : numNeighbourColumn.getValues(Integer.class)){
			if (numNeighbour > maxNumNeighbor) maxNumNeighbor = numNeighbour;
		}
		if (maxNumNeighbor > 1000){
			maxNumNeighbor = 1000;
		}
		
		maxLogPVal = 0;
		CyColumn logPValColumn = network.getDefaultNodeTable().getColumn(ConstructNetworkTask.NODE_LOG_PVALUE);
		for (Double logPVal : logPValColumn.getValues(Double.class)){
			if (logPVal > maxLogPVal) maxLogPVal = logPVal;
		}

		
		discreteMappingFactory = appAdapter.getVisualMappingFunctionDiscreteFactory();
		continuousMappingFactory = appAdapter.getVisualMappingFunctionContinuousFactory();
		visualStyle = appAdapter.getVisualMappingManager().getCurrentVisualStyle();
		

		highlightContextualNodesButton.setEnabled(true);
		highlightContextualNodesButton.setActionCommand("highlightContextualNodesButtonActionListener");
		highlightContextualNodesButton.addActionListener(this);
		
		// toggle buttons for sizing the nodes by the number of their contextual neighbors
		contextualNeighborsButton.setEnabled(true);
		contextualNeighborsButton.addActionListener(this);
		contextualNeighborsButton.setActionCommand("contextualNeighborsButtonActionListener");;
		contextualNeighborsButton.setSelected(true);
		this.createVisualStyleForContextualHubs(); //default network design when network is created
		degreeButton.setSelected(false);
		
		degreeButton.setEnabled(true);
		degreeButton.addActionListener(this);
		degreeButton.setActionCommand("degreeButtonActionListener");

		
		DefaultTableModel colorTableModel = new DefaultTableModel();
		colorTableModel.setColumnIdentifiers(new String[]{"Feature", "Color"});
		JTable colorTable = new JTable(){
			public boolean isCellEditable(int rowIndex, int columnIndex) {
				if (columnIndex == 0){
					return false;	
				}else{
					return true;
				}
			}
		};
		
		colorTable.setModel(colorTableModel);
		
		colorTableModel.addRow(new Object[]{"Hub colour gradient - max", new JButton()});
		colorTableModel.addRow(new Object[]{"Hub colour gradient - min", new JButton()});
		colorTableModel.addRow(new Object[]{"Contextual node colour"});
		colorTableModel.addRow(new Object[]{"Non-contextual node colour"});
		
		colorTable.getColumn("Color").setCellEditor(new ColorTableCellEditorRenderer());
		colorTable.getColumn("Color").setCellRenderer(new ColorTableCellEditorRenderer());
		
		colorTable.setGridColor(new Color(192,192,192));
		colorTable.setShowHorizontalLines(true);
		colorTable.setShowVerticalLines(false);
		
		colorTable.setRowHeight(30);
		colorTable.setIntercellSpacing(new Dimension(10, 0));
		colorTable.getColumn("Feature").setPreferredWidth(160);
		colorTable.getColumn("Color").setPreferredWidth(40);
		colorTable.setBorder(new MatteBorder(1, 1, 1, 1, new Color(0, 0, 0)));

		colorTable.setRowSelectionAllowed(false);
		
		/**
		 * Reduce the size of the network by adjusting the slider
		 */
		sliderPanel = new JPanel();
		sliderPanel.setBorder(new TitledBorder("Visualize the top n hubs"));
		
		// slider to look at only the top x nodes
		slider = new JSlider(JSlider.HORIZONTAL, 0, 40, DEFAULTNUMBEROFNODESSHOWN);
		slider.setMajorTickSpacing(10);
		slider.setMinorTickSpacing(1);
		slider.setPaintTicks(true);
		slider.setPaintLabels(true);
		slider.setSnapToTicks(true);
		
		sliderValueField = new JTextField(Integer.toString(DEFAULTNUMBEROFNODESSHOWN),3);
		sliderValueField.addActionListener(this); // if immediate change wanted: .addChangeListener
		sliderValueField.setActionCommand("sliderValueFieldActionListener");
		
		errorLabelForSliderValue = new JLabel("Positive numeric values only please!");
		errorLabelForSliderValue.setVisible(false);
		errorLabelForSliderValue.setForeground(Color.RED);
		

		slider.addChangeListener(new ChangeListener(){
			public void stateChanged(ChangeEvent e){
				
				JSlider sliderSource = (JSlider)e.getSource();
				
				if (!sliderSource.getValueIsAdjusting()){
					// get the number of nodes the user wants to view
					if (maxNumberNodesShown != (int)sliderSource.getValue()){
						if (maxNumberNodesShown <= slider.getMaximum()){
							maxNumberNodesShown = (int)sliderSource.getValue();
							sliderValueField.setText(Integer.toString(maxNumberNodesShown));
						}
					}
					if (slider.getValue() < slider.getMaximum()){
						maxNumberNodesShown = (int)sliderSource.getValue();
						sliderValueField.setText(Integer.toString(maxNumberNodesShown));
					}

					//compute node ranking //TODO this could be done outside this function but then requires a change listener for node deletion, which could also recolor the nodes
					if (contextualNeighborsButton.isSelected()){
						pValueNodeRank = computeNodeRankingBasedOnPValue();
						// hide all nodes that are not in the top x nodes
						hideAllExceptTopXNodesAndNeighbors(pValueNodeRank, maxNumberNodesShown);
					}
					else{
						neighborsNodeRank = computeNodeRankingBasedOnDegree();
						// hide all nodes that are not in the top x nodes
						hideAllExceptTopXNodesAndNeighborsInt(neighborsNodeRank, maxNumberNodesShown);
					}
					
				}
			}
		});


		layoutComboBox = new JComboBox<CyLayoutAlgorithm>();
		Collection<CyLayoutAlgorithm> possibleLayouts = appAdapter.getCyLayoutAlgorithmManager().getAllLayouts();
		for (CyLayoutAlgorithm layoutAlgo : possibleLayouts){
			layoutComboBox.addItem(layoutAlgo);
		}
		layoutComboBox.setSelectedItem(appAdapter.getCyLayoutAlgorithmManager().getLayout("kamada-kawai"));


		reLayoutButton = new JButton("Re-layout network using:");
		reLayoutButton.addActionListener(new ActionListener(){
			/**
			 * Applies a network to the visible nodes
			 */
			@Override
			public void actionPerformed(ActionEvent e) {
				CyNetworkView networkView = appAdapter.getCyNetworkViewManager().getNetworkViews(network).iterator().next();
				
				CyLayoutAlgorithm newLayout = (CyLayoutAlgorithm) layoutComboBox.getSelectedItem();
				Collection<View<CyNode>> allCyNodes = networkView.getNodeViews();
				Set<View<CyNode>> cyNodeSetToRelayout = new HashSet<View<CyNode>>();
				for (View<CyNode> cyNodeView : allCyNodes){
					Boolean visible = cyNodeView.getVisualProperty(BasicVisualLexicon.NODE_VISIBLE);
					if (visible){
						cyNodeSetToRelayout.add(cyNodeView);	
					}
				}
				TaskIterator taskIterator = newLayout.createTaskIterator(networkView, newLayout.createLayoutContext(), cyNodeSetToRelayout, null);
				appAdapter.getTaskManager().execute(taskIterator);
			}
		});

		colorSchemaBorderPanel = new JPanel();
		colorSchemaBorderPanel.setBorder(new TitledBorder("Change the colour schema"));
		
		nodeAnalyzerBorderPanel = new JPanel();
		nodeAnalyzerBorderPanel.setBorder(new TitledBorder("Node Analyzer"));
				
			
		/**
		 * Layout of components on the resultPanel
		 */
		
		GroupLayout groupLayout = new GroupLayout(viewPortPanel);
		viewPortPanel.setLayout(groupLayout);
		
		groupLayout.setAutoCreateGaps(true);
		groupLayout.setAutoCreateContainerGaps(true);
		
		groupLayout.setHorizontalGroup(
				groupLayout.createSequentialGroup()
				.addGroup(
						groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
						.addComponent(dbAndSpeciesPanel, GroupLayout.Alignment.LEADING)
						.addComponent(modifyNetworkDesignPanel, GroupLayout.Alignment.LEADING)
						.addComponent(sliderPanel, GroupLayout.Alignment.LEADING)
						.addComponent(colorSchemaBorderPanel, GroupLayout.Alignment.LEADING)
						.addComponent(nodeAnalyzerBorderPanel, GroupLayout.Alignment.LEADING)));
				

		groupLayout.setVerticalGroup(
				groupLayout.createSequentialGroup()
				.addComponent(dbAndSpeciesPanel)
				.addComponent(modifyNetworkDesignPanel)
				.addComponent(sliderPanel)
				.addComponent(colorSchemaBorderPanel)
				.addComponent(nodeAnalyzerBorderPanel));
		
		// database and species panel
		
		GridBagLayout gbl_dbAndSpeciesPanel = new GridBagLayout();
		gbl_dbAndSpeciesPanel.columnWidths = new int[]{0, 50, 0};
		gbl_dbAndSpeciesPanel.columnWeights = new double[]{0.0, 0.0, 1.0};
		dbAndSpeciesPanel.setLayout(gbl_dbAndSpeciesPanel);
		
		
		GridBagConstraints gbc_databaseLabel = new GridBagConstraints();
		gbc_databaseLabel.fill = GridBagConstraints.BOTH;
		gbc_databaseLabel.insets = new Insets(5, 5, 5, 5);
		gbc_databaseLabel.anchor = GridBagConstraints.NORTHWEST;
		gbc_databaseLabel.gridx = 0;
		gbc_databaseLabel.gridy = 0;
		dbAndSpeciesPanel.add(databaseLabel, gbc_databaseLabel);
 		
		GridBagConstraints gbc_speciesLabel = new GridBagConstraints();
		gbc_speciesLabel.insets = new Insets(0, 5, 5, 5);
		gbc_speciesLabel.anchor = GridBagConstraints.NORTHWEST;
		gbc_speciesLabel.gridx = 0;
		gbc_speciesLabel.gridy = 1;
		dbAndSpeciesPanel.add(speciesLabel, gbc_speciesLabel);

		GridBagConstraints gbc_idTypeLabel = new GridBagConstraints();
		gbc_idTypeLabel.insets = new Insets(0, 5, 5, 5);
		gbc_idTypeLabel.anchor = GridBagConstraints.NORTHWEST;
		gbc_idTypeLabel.gridx = 0;
		gbc_idTypeLabel.gridy = 2;
		dbAndSpeciesPanel.add(idTypeLabel, gbc_idTypeLabel);

		GridBagConstraints gbc_interactionTypeLabel = new GridBagConstraints();
		gbc_interactionTypeLabel.insets = new Insets(5, 5, 5, 5);
		gbc_interactionTypeLabel.anchor = GridBagConstraints.NORTHWEST;
		gbc_interactionTypeLabel.gridx = 2;
		gbc_interactionTypeLabel.gridy = 0;
		dbAndSpeciesPanel.add(interactionTypeLabel, gbc_interactionTypeLabel);
		
		interactionTypeListScrollPane = new JScrollPane();
		GridBagConstraints gbc_interactionTypeListScrollPane = new GridBagConstraints();
		gbc_interactionTypeListScrollPane.gridheight = 2;
		gbc_interactionTypeListScrollPane.insets = new Insets(0, 5, 5, 5);
		gbc_interactionTypeListScrollPane.anchor = GridBagConstraints.NORTHWEST;
		gbc_interactionTypeListScrollPane.gridx = 2;
		gbc_interactionTypeListScrollPane.gridy = 1;
		dbAndSpeciesPanel.add(interactionTypeListScrollPane, gbc_interactionTypeListScrollPane);
		interactionTypeListScrollPane.setViewportView(interactionList);

		// hubs view and highlighting of contextual nodes
		
		GridBagLayout gbl_modifyNetworkDesignPanel = new GridBagLayout();
		gbl_modifyNetworkDesignPanel.columnWidths = new int[] {200, 80, 100};
		gbl_modifyNetworkDesignPanel.columnWeights = new double[] {0.0, 0.0, 1.0};
		modifyNetworkDesignPanel.setLayout(gbl_modifyNetworkDesignPanel);
		
		
		GridBagConstraints gbc_contextualNeighborsButton = new GridBagConstraints();
		gbc_contextualNeighborsButton.anchor = GridBagConstraints.NORTHWEST;
		gbc_contextualNeighborsButton.insets = new Insets(0, 0, 5, 5);
		gbc_contextualNeighborsButton.gridx = 0;
		gbc_contextualNeighborsButton.gridy = 0;
		modifyNetworkDesignPanel.add(contextualNeighborsButton, gbc_contextualNeighborsButton);
		
		
		GridBagConstraints gbc_degreeButton = new GridBagConstraints();
		gbc_degreeButton.anchor = GridBagConstraints.NORTHWEST;
		gbc_degreeButton.insets = new Insets(0, 0, 5, 5);
		gbc_degreeButton.gridx = 1;
		gbc_degreeButton.gridy = 0;
		modifyNetworkDesignPanel.add(degreeButton, gbc_degreeButton);
		
		
		GridBagConstraints gbc_highlightContextualNodesButton = new GridBagConstraints();
		gbc_highlightContextualNodesButton.anchor = GridBagConstraints.NORTHWEST;
		gbc_highlightContextualNodesButton.insets = new Insets(0, 0, 5, 5);
		gbc_highlightContextualNodesButton.gridx = 0;
		gbc_highlightContextualNodesButton.gridy = 1;
		modifyNetworkDesignPanel.add(highlightContextualNodesButton, gbc_highlightContextualNodesButton);
		
		
		// color schema
		GridBagLayout gbl_colorSchemaBorderPanel = new GridBagLayout();
		gbl_colorSchemaBorderPanel.columnWidths = new int[] {200, 80, 200};
		gbl_colorSchemaBorderPanel.columnWeights = new double[] {0.0, 0.0, 1.0};
		colorSchemaBorderPanel.setLayout(gbl_colorSchemaBorderPanel);
		
		GridBagConstraints gbc_colorTable = new GridBagConstraints();
		gbc_colorTable.anchor = GridBagConstraints.NORTHWEST;
		gbc_colorTable.insets = new Insets(5, 5, 5, 5);
		gbc_colorTable.gridx = 0;
		gbc_colorTable.gridy = 0;
		colorSchemaBorderPanel.add(colorTable, gbc_colorTable);

		
		// slider to limit network to the most important nodes and their neighbors
		
		GridBagLayout gbl_sliderPanel = new GridBagLayout();
		gbl_sliderPanel.columnWidths = new int[] {200, 80, 200};
		gbl_sliderPanel.columnWeights = new double[] {0.0, 0.0, 1.0};
		sliderPanel.setLayout(gbl_sliderPanel);
		
		
		sliderInstructionLabel = new JLabel("Use the slider to only show the top n hubs and their first neighbors.");
		GridBagConstraints gbc_sliderInstructionLabel = new GridBagConstraints();
		gbc_sliderInstructionLabel.anchor = GridBagConstraints.WEST;
		gbc_sliderInstructionLabel.gridwidth = 3;
		gbc_sliderInstructionLabel.insets = new Insets(5, 5, 5, 5);
		gbc_sliderInstructionLabel.gridx = 0;
		gbc_sliderInstructionLabel.gridy = 0;
		sliderPanel.add(sliderInstructionLabel, gbc_sliderInstructionLabel);
		

		GridBagConstraints gbc_slider = new GridBagConstraints();
		gbc_slider.gridwidth = 2;
		gbc_slider.anchor = GridBagConstraints.NORTHWEST;
		gbc_slider.insets = new Insets(0, 0, 5, 5);
		gbc_slider.gridx = 0;
		gbc_slider.gridy = 1;
		sliderPanel.add(slider, gbc_slider);
		
		
		GridBagConstraints gbc_sliderValueField = new GridBagConstraints();
		gbc_sliderValueField.anchor = GridBagConstraints.NORTHWEST;
		gbc_sliderValueField.insets = new Insets(0, 0, 5, 5);
		gbc_sliderValueField.gridx = 1;
		gbc_sliderValueField.gridy = 1;
		sliderPanel.add(sliderValueField, gbc_sliderValueField);
		
		
		GridBagConstraints gbc_errorLabelForSliderValue = new GridBagConstraints();
		gbc_errorLabelForSliderValue.anchor = GridBagConstraints.NORTHWEST;
		gbc_errorLabelForSliderValue.insets = new Insets(0, 0, 5, 5);
		gbc_errorLabelForSliderValue.gridx = 2;
		gbc_errorLabelForSliderValue.gridy = 1;
		sliderPanel.add(errorLabelForSliderValue, gbc_errorLabelForSliderValue);

		
		GridBagConstraints gbc_reLayoutButton = new GridBagConstraints();
		gbc_reLayoutButton.anchor = GridBagConstraints.NORTHWEST;
		gbc_reLayoutButton.insets = new Insets(0, 0, 5, 5);
		gbc_reLayoutButton.gridx = 0;
		gbc_reLayoutButton.gridy = 2;
		sliderPanel.add(reLayoutButton, gbc_reLayoutButton);

		GridBagConstraints gbc_layoutComboBox = new GridBagConstraints();
		gbc_layoutComboBox.anchor = GridBagConstraints.NORTHWEST;
		gbc_layoutComboBox.insets = new Insets(0, 0, 5, 5);
		gbc_layoutComboBox.gridwidth = 2;
		gbc_layoutComboBox.gridx = 1;
		gbc_layoutComboBox.gridy = 2;
		sliderPanel.add(layoutComboBox, gbc_layoutComboBox);


		// node analyzer

		GridBagLayout gbl_nodeAnalyzerBorderPanel = new GridBagLayout();
		gbl_nodeAnalyzerBorderPanel.columnWeights = new double[] {0.0, 1.0};
		nodeAnalyzerBorderPanel.setLayout(gbl_nodeAnalyzerBorderPanel);
		
		
		legendLabel = new JLabel("To analyze a node, right-click it, choose 'Apps' and then 'CHAT Node Analyzer'");
		GridBagConstraints gbc_legendLabel = new GridBagConstraints();
		gbc_legendLabel.insets = new Insets(5, 5, 5, 5);
		gbc_legendLabel.anchor = GridBagConstraints.NORTHWEST;
		gbc_legendLabel.gridx = 0;
		gbc_legendLabel.gridy = 0;
		nodeAnalyzerBorderPanel.add(legendLabel, gbc_legendLabel);
		
		
		nodeAnalyzerPanel = new NodeAnalyzerPanel(appAdapter, network, this);
		GridBagConstraints gbc_nodeAnalyzerPanel = new GridBagConstraints();
		gbc_nodeAnalyzerPanel.gridwidth = 3;
		gbc_nodeAnalyzerPanel.anchor = GridBagConstraints.WEST;
		gbc_nodeAnalyzerPanel.gridx = 0;
		gbc_nodeAnalyzerPanel.gridy = 1;
		nodeAnalyzerBorderPanel.add(nodeAnalyzerPanel, gbc_nodeAnalyzerPanel);
		
		nodeAnalyzerPanel.setCollapsed(false);
		
		setViewportView(viewPortPanel);
	}


	public NodeAnalyzerPanel getNodeAnalyzerPanel(){
		return this.nodeAnalyzerPanel;
	}
	
	@Override
	public Component getComponent() {
		return this;
	}

	@Override
	public CytoPanelName getCytoPanelName() {
		return CytoPanelName.EAST;
	}

	@Override
	public String getTitle() {
		return networkName;
	}

	@Override
	public Icon getIcon() {
		return null;
	}


	@Override
	public void actionPerformed(ActionEvent e) {
		/**
		 * Click actions of the buttons.
		 */
		
		/**
		 * Highlight contextual nodes and gray out the other nodes.
		 */
		if (e.getActionCommand() == "highlightContextualNodesButtonActionListener"){
			if (highlightContextualNodesButton.isSelected()){
				//highlight contextually important nodes
				createContextualityMapping();
			}else{
				//remove node color mapping and add the button specific one
				visualStyle.removeVisualMappingFunction(BasicVisualLexicon.NODE_FILL_COLOR);
				
				//set node color shading according to the button property
				if (contextualNeighborsButton.isSelected()){	
					this.nodeColorShadesBasedOnPValue();
				}else if (degreeButton.isSelected()){
					this.nodeColorShadesBasedOnDegree();
				}
			}
		}

		
		/**
		 * Set label size, color and size nodes according to the node's negative log p-value
		 */
		else if (e.getActionCommand() =="contextualNeighborsButtonActionListener"){
			
			this.createVisualStyleForContextualHubs();
			
			// hide all nodes that are not in the top x nodes
			pValueNodeRank = computeNodeRankingBasedOnPValue();	
			hideAllExceptTopXNodesAndNeighbors(pValueNodeRank, maxNumberNodesShown);
			
			//make this a toggle button
			degreeButton.setSelected(false);
		}
		

		/**
		 * Set label size, color and size nodes according to the node's contextual neighbors
		 */
		else if (e.getActionCommand() == "degreeButtonActionListener"){
			
			createDegreeHubMapping();
			
			// hide all nodes that are not in the top x nodes
			neighborsNodeRank = computeNodeRankingBasedOnDegree();	
			hideAllExceptTopXNodesAndNeighborsInt(neighborsNodeRank, maxNumberNodesShown);
						
			//make this a toggle button
			contextualNeighborsButton.setSelected(false);
		}

		
		/**
		 * Update slider based on user input in text field, check validity of user input,
		 * trigger network visualization update based on the top x nodes.
		 */
		else if (e.getActionCommand() == "sliderValueFieldActionListener"){
			try {
				maxNumberNodesShown = Integer.parseInt(sliderValueField.getText());
				
				if (maxNumberNodesShown >= 0){
					//update slider value
					errorLabelForSliderValue.setVisible(false);
					if (slider.getValue() != maxNumberNodesShown){
						if (maxNumberNodesShown > slider.getMaximum()){
							if (slider.getValue() != slider.getMaximum()){
								slider.setValue(slider.getMaximum()); //slider's listener will still use maxNumberNodesShown
							}else{
								//since slider's position don't change, we have to do the calculation here
								if (contextualNeighborsButton.isSelected()){
									pValueNodeRank = computeNodeRankingBasedOnPValue();
									// hide all nodes that are not in the top x nodes
									hideAllExceptTopXNodesAndNeighbors(pValueNodeRank, maxNumberNodesShown);
								}
								else{
									neighborsNodeRank = computeNodeRankingBasedOnDegree();
									// hide all nodes that are not in the top x nodes
									hideAllExceptTopXNodesAndNeighborsInt(neighborsNodeRank, maxNumberNodesShown);
								}
							}
						}else{
							slider.setValue(maxNumberNodesShown);	
						}
					}
					
			//display warning if user inputs any other character other than positive integers		
				}else{
					errorLabelForSliderValue.setVisible(true);	
				}
				
			}
			catch(NumberFormatException nfe){
				errorLabelForSliderValue.setVisible(true);
			}
		}
	}


	public ArrayList<SortableNodeObject> computeNodeRankingBasedOnPValue(){
		/**
		 * Create a ranked map of the node's p-values (with associated SUIDs) 
		 */
		CyTable nodeTable = network.getDefaultNodeTable();
		List<?> SUIDList = nodeTable.getColumn("SUID").getValues(nodeTable.getColumn("SUID").getType());
		List<?> pValueList = nodeTable.getColumn("-log10PV").getValues(nodeTable.getColumn("-log10PV").getType());
		
		ArrayList <SortableNodeObject> pValueToSUIDList= new ArrayList<SortableNodeObject>();
		for (int i = 0; i < pValueList.size(); i++){
			Long SUID = (Long) SUIDList.get(i);
			Double pValue = (Double) pValueList.get(i);
			SortableNodeObject curObj = new SortableNodeObject(SUID, pValue); 
			pValueToSUIDList.add(curObj);
		}
		
		Collections.sort(pValueToSUIDList, Collections.reverseOrder());
		
		return pValueToSUIDList;
		
	}
	
	public ArrayList<SortableNodeObjectInt> computeNodeRankingBasedOnDegree(){
		/**
		 * Create a ranked map of the node's degree (with associated SUIDs) 
		 */
		CyTable nodeTable = network.getDefaultNodeTable();
		List<?> SUIDList = nodeTable.getColumn("SUID").getValues(nodeTable.getColumn("SUID").getType());
		List<?> numNeighborsList = nodeTable.getColumn("Total Neighbours in DB").getValues(nodeTable.getColumn("Total Neighbours in DB").getType());
		
		ArrayList <SortableNodeObjectInt> numNeighborsToSUIDList = new ArrayList<SortableNodeObjectInt>();
		for (int i = 0; i < numNeighborsList.size(); i++){
			Long SUID = (Long) SUIDList.get(i);
			Integer numNeighbors = (Integer) numNeighborsList.get(i);
			SortableNodeObjectInt currentObj = new SortableNodeObjectInt(SUID, numNeighbors);
			numNeighborsToSUIDList.add(currentObj);
		}

		Collections.sort(numNeighborsToSUIDList, Collections.reverseOrder());
		
		return numNeighborsToSUIDList;
		
	}
	
	protected class SortableNodeObject implements Comparable<Object>{
		/**
		 * Object to sort SUIDs by pvalue
		 */
		private Long SUID;
		private Double pVal;
		
		private SortableNodeObject(Long SUID, Double pVal){
			this.SUID = SUID;
			this.pVal = pVal;
		}
		protected Long getSUID(){
			return this.SUID;
		}
		protected Double getPValue(){
			return this.pVal;
		}
		
		@Override
		public int compareTo(Object o) {
			SortableNodeObject oo = (SortableNodeObject)o;
			return this.pVal.compareTo(oo.getPValue());
		}
		
		@Override
		public boolean equals(Object o){
			SortableNodeObject oo = (SortableNodeObject)o;
			if(oo.SUID.equals(SUID)){
				return true;
			}
			else{
				return false;
			}
		}

		@Override
		public String toString(){
			return "SUID: "+SUID + ", p-value: " + pVal;
		}
	}
	
	protected class SortableNodeObjectInt implements Comparable<Object>{
		/**
		 * Object to sort SUIDs by number of neighbors
		 */
		private Long SUID;
		private Integer numNeighbors;
		
		private SortableNodeObjectInt(Long SUID, Integer numNeighbors){
			this.SUID = SUID;
			this.numNeighbors = numNeighbors;
		}
		protected Long getSUID(){
			return this.SUID;
		}
		protected int getNumNeighbors(){
			return (int) this.numNeighbors;
		}
		
		@Override
		public int compareTo(Object o) {
			SortableNodeObjectInt oo = (SortableNodeObjectInt)o;
			return this.numNeighbors.compareTo(oo.getNumNeighbors());
		}
		
		@Override
		public boolean equals(Object o){
			SortableNodeObjectInt oo = (SortableNodeObjectInt)o;
			if(oo.SUID.equals(SUID)){
				return true;
			}
			else{
				return false;
			}
		}

		@Override
		public String toString(){
			return "SUID: "+SUID + ", number of neighbors: " + numNeighbors;
		}
	}
	
	public void hideAllExceptTopXNodesAndNeighbors(ArrayList<SortableNodeObject> lst, Integer topXNodes){
		/**
		 * Makes the top X nodes and their first neighbors and adjacent edges visible
		 */
		CyNetworkView networkView = appAdapter.getCyNetworkViewManager().getNetworkViews(network).iterator().next();
		
		
		int numberOfNodesInNetwork = network.getNodeCount();
		if (topXNodes > numberOfNodesInNetwork) topXNodes = numberOfNodesInNetwork;
		
		
		//hide all nodes and edges
		for (View<CyEdge> edgeView : networkView.getEdgeViews()){
			edgeView.setLockedValue(BasicVisualLexicon.EDGE_VISIBLE, false);
		}
		for (View<CyNode> nodeView : networkView.getNodeViews()){
			nodeView.setLockedValue(BasicVisualLexicon.NODE_VISIBLE, false);
		}
		
		System.out.println("Getting first neighbors of top " + topXNodes + " nodes and hiding all other nodes.");
		
		// show only nodes that are in the top X nodes or are their neighbors
		for (int i=0; i < topXNodes; i ++){
			
			CyNode aTopXNode = network.getNode(lst.get(i).getSUID());
			showNode(aTopXNode, networkView);
			List<CyNode> firstNeighbors = network.getNeighborList(aTopXNode, Type.ANY);
			for (CyNode firstNeighbor : firstNeighbors){
				showNode(firstNeighbor, networkView);
				showAdjacentEdges(firstNeighbor, networkView);
			}
		}
		
		
		networkView.updateView();
		
		
		//deselect hidden nodes and edges, make sure selected nodes/edges that are visible are shown as selected
		for (View<CyEdge> edgeView : networkView.getEdgeViews()){
			if (!edgeView.getVisualProperty(BasicVisualLexicon.EDGE_VISIBLE)){
				network.getRow(edgeView.getModel()).set(CyNetwork.SELECTED, false);
			}else if (network.getRow(edgeView.getModel()).get(CyNetwork.SELECTED, Boolean.class)){
				network.getRow(edgeView.getModel()).set(CyNetwork.SELECTED, true);
			}
		}
		for (View<CyNode> nodeView : networkView.getNodeViews()){
			if (!nodeView.getVisualProperty(BasicVisualLexicon.NODE_VISIBLE)){
				network.getRow(nodeView.getModel()).set(CyNetwork.SELECTED, false);
			}else if (network.getRow(nodeView.getModel()).get(CyNetwork.SELECTED, Boolean.class)){
				network.getRow(nodeView.getModel()).set(CyNetwork.SELECTED, true);
			}
		}
	}
	
	public void hideAllExceptTopXNodesAndNeighborsInt(ArrayList<SortableNodeObjectInt> lst, Integer topXNodes){
		/**
		 * Makes the top X nodes and their first neighbors and adjacent edges visible
		 */
		CyNetworkView networkView = appAdapter.getCyNetworkViewManager().getNetworkViews(network).iterator().next();
		
		int numberOfNodesInNetwork = network.getNodeCount();
		if (topXNodes > numberOfNodesInNetwork) topXNodes = numberOfNodesInNetwork;
		
		
		//hide all nodes and edges
		for (View<CyEdge> edgeView : networkView.getEdgeViews()){
			edgeView.setLockedValue(BasicVisualLexicon.EDGE_VISIBLE, false);
		}
		for (View<CyNode> nodeView : networkView.getNodeViews()){
			nodeView.setLockedValue(BasicVisualLexicon.NODE_VISIBLE, false);
		}
		
		System.out.println("Getting first neighbors of top " + topXNodes + " nodes and hiding all other nodes.");
		
		// show only nodes that are in the top X nodes or are their neighbors
		for (int i=0; i < topXNodes; i ++){
			
			CyNode aTopXNode = network.getNode(lst.get(i).getSUID());
			showNode(aTopXNode, networkView);
			List<CyNode> firstNeighbors = network.getNeighborList(aTopXNode, Type.ANY);
			for (CyNode firstNeighbor : firstNeighbors){
				showNode(firstNeighbor, networkView);
				showAdjacentEdges(firstNeighbor, networkView);
			}
		}
		
		
		networkView.updateView();
		
		
		//deselect hidden nodes and edges, make sure selected nodes/edges that are visible are shown as selected
		for (View<CyEdge> edgeView : networkView.getEdgeViews()){
			if (!edgeView.getVisualProperty(BasicVisualLexicon.EDGE_VISIBLE)){
				network.getRow(edgeView.getModel()).set(CyNetwork.SELECTED, false);
			}else if (network.getRow(edgeView.getModel()).get(CyNetwork.SELECTED, Boolean.class)){
				network.getRow(edgeView.getModel()).set(CyNetwork.SELECTED, true);
			}
		}
		for (View<CyNode> nodeView : networkView.getNodeViews()){
			if (!nodeView.getVisualProperty(BasicVisualLexicon.NODE_VISIBLE)){
				network.getRow(nodeView.getModel()).set(CyNetwork.SELECTED, false);
			}else if (network.getRow(nodeView.getModel()).get(CyNetwork.SELECTED, Boolean.class)){
				network.getRow(nodeView.getModel()).set(CyNetwork.SELECTED, true);
			}
		}
	}
	
	private void showNode(CyNode node, CyNetworkView networkView){
		/**
		 * Make a node visible.
		 */
		View<CyNode> nodeView = networkView.getNodeView(node);
		nodeView.clearValueLock(BasicVisualLexicon.NODE_VISIBLE);
		showAdjacentEdges(node, networkView);
	}
	
	
	private void showAdjacentEdges(CyNode node, CyNetworkView networkView){
		/**
		 * Make adjacent edges of a node visible.
		 */
		List<CyEdge> adjacentEdges = network.getAdjacentEdgeList(node, CyEdge.Type.ANY);
		for (CyEdge adjacentEdge : adjacentEdges){
			View<CyEdge> adjacentEdgeView = networkView.getEdgeView(adjacentEdge);
			adjacentEdgeView.clearValueLock(BasicVisualLexicon.EDGE_VISIBLE);
		}
	}
	

	private void createContextualityMapping(){
		/**
		 * Set up the visual style for showing contextual vs non-contextual nodes
		 */
		DiscreteMapping<Boolean, Paint> nodeFillColorDiscreteMapping = (DiscreteMapping<Boolean, Paint>) discreteMappingFactory.createVisualMappingFunction(ConstructNetworkTask.NODE_CONTEXTUALLY_IMPORTANT, Boolean.class, BasicVisualLexicon.NODE_FILL_COLOR);
		nodeFillColorDiscreteMapping.putMapValue(true, contextualColor);
		nodeFillColorDiscreteMapping.putMapValue(false, notContextualColor);
		visualStyle.addVisualMappingFunction(nodeFillColorDiscreteMapping);	
	}
	
	private void createDegreeHubMapping(){
		/**
		 * Set up the visual style for showing degree hubs.
		 */
		//set node size according to the number of contextual neighbors
		ContinuousMapping<Integer, Double> numNeighbourSize = (ContinuousMapping<Integer, Double>)continuousMappingFactory.createVisualMappingFunction(ConstructNetworkTask.NODE_TOTAL_NEIGHBOURS, Integer.class, BasicVisualLexicon.NODE_SIZE);
		numNeighbourSize.addPoint(0, new BoundaryRangeValues<Double>(30.0, 30.0, 30.0));
		numNeighbourSize.addPoint(maxNumNeighbor, new BoundaryRangeValues<Double>(400.0, 400.0, 400.0));
		visualStyle.addVisualMappingFunction(numNeighbourSize);
		
		//set label size according to the number of contextual neighbors
		ContinuousMapping<Integer, Integer> contextualNeighbourLabelSize = (ContinuousMapping<Integer, Integer>) continuousMappingFactory.createVisualMappingFunction(ConstructNetworkTask.NODE_TOTAL_NEIGHBOURS, Integer.class, BasicVisualLexicon.NODE_LABEL_FONT_SIZE);
		contextualNeighbourLabelSize.addPoint(0, new BoundaryRangeValues<Integer>(15, 15, 15));
		contextualNeighbourLabelSize.addPoint(maxNumNeighbor, new BoundaryRangeValues<Integer>(100, 100, 100));
		visualStyle.addVisualMappingFunction(contextualNeighbourLabelSize);
		
		//set node color shading according to the number of contextual neighbors
		if (!highlightContextualNodesButton.isSelected()){
			this.nodeColorShadesBasedOnDegree();
		}
	}
	

	
	private void createVisualStyleForContextualHubs(){
		/**
		 * Set up the visual style for showing contextual hubs.
		 */
		//set node size according to the negative log p-value
		ContinuousMapping<Double, Double> logPValSize = (ContinuousMapping<Double, Double>)continuousMappingFactory.createVisualMappingFunction(ConstructNetworkTask.NODE_LOG_PVALUE, Double.class, BasicVisualLexicon.NODE_SIZE);
		logPValSize.addPoint(0.0, new BoundaryRangeValues<Double>(30.0, 30.0, 30.0));
		logPValSize.addPoint(maxLogPVal, new BoundaryRangeValues<Double>(400.0, 400.0, 400.0));
		visualStyle.addVisualMappingFunction(logPValSize);
		
		//set label size according to the negative log p-value
		ContinuousMapping<Double, Integer> pValueLabelSize = (ContinuousMapping<Double, Integer>) continuousMappingFactory.createVisualMappingFunction(ConstructNetworkTask.NODE_LOG_PVALUE, Double.class, BasicVisualLexicon.NODE_LABEL_FONT_SIZE);
		pValueLabelSize.addPoint(0.0, new BoundaryRangeValues<Integer>(15, 15, 15));
		pValueLabelSize.addPoint(maxLogPVal, new BoundaryRangeValues<Integer>(100, 100, 100));
		visualStyle.addVisualMappingFunction(pValueLabelSize);
		
		//set node color shading according to the negative log p-value
		if (!highlightContextualNodesButton.isSelected()){
			this.nodeColorShadesBasedOnPValue();				
		}
	}

	
	private void nodeColorShadesBasedOnPValue(){
		/**
		 * Set node color shading according to the negative log p-value
		 */
		ContinuousMapping<Double, Paint> pValueNodeColor = (ContinuousMapping<Double, Paint>)continuousMappingFactory.createVisualMappingFunction(ConstructNetworkTask.NODE_LOG_PVALUE, Double.class, BasicVisualLexicon.NODE_FILL_COLOR);
		pValueNodeColor.addPoint(0.0, new BoundaryRangeValues<Paint>(lowerColorShade, lowerColorShade, lowerColorShade));
		pValueNodeColor.addPoint(maxLogPVal, new BoundaryRangeValues<Paint>(upperColorShade, upperColorShade, upperColorShade));
		visualStyle.addVisualMappingFunction(pValueNodeColor);	
	}

	
	private void nodeColorShadesBasedOnDegree(){
		/**
		 * Set node color shading according to the number of contextual neighbors
		 */
		ContinuousMapping<Integer, Paint> numNeighborColor = (ContinuousMapping<Integer, Paint>)continuousMappingFactory.createVisualMappingFunction(ConstructNetworkTask.NODE_TOTAL_NEIGHBOURS, Integer.class, BasicVisualLexicon.NODE_FILL_COLOR);
		numNeighborColor.addPoint(0, new BoundaryRangeValues<Paint>(lowerColorShade, lowerColorShade, lowerColorShade));
		numNeighborColor.addPoint(maxNumNeighbor, new BoundaryRangeValues<Paint>(upperColorShade, upperColorShade, upperColorShade));
		visualStyle.addVisualMappingFunction(numNeighborColor);
	}
	
	protected void disableSlider(){
		/**
		 * Disable the slider and its related components.
		 */
		slider.setEnabled(false);
		sliderValueField.setEnabled(false);
		sliderInstructionLabel.setEnabled(false);
	}
	
	protected void enableSlider(){
		/**
		 * Enable the slider and its related components.
		 */
		slider.setEnabled(true);
		sliderValueField.setEnabled(true);
		sliderInstructionLabel.setEnabled(true);
	}
	
	protected void nodeAnalyzerExited(){
		
		if (contextualNeighborsButton.isSelected()){
			pValueNodeRank = computeNodeRankingBasedOnPValue();
			// hide all nodes that are not in the top x nodes
			hideAllExceptTopXNodesAndNeighbors(pValueNodeRank, maxNumberNodesShown);
		}
		else{
			neighborsNodeRank = computeNodeRankingBasedOnDegree();
			// hide all nodes that are not in the top x nodes
			hideAllExceptTopXNodesAndNeighborsInt(neighborsNodeRank, maxNumberNodesShown);
		}
		
		enableSlider();
	}
	

	class ColorTableCellEditorRenderer extends JButton implements TableCellEditor, TableCellRenderer{
		/**
		 * table cell renderer and editor that allows the user to pick a color in the color
		 * chooser window which is then applied to the layout, e.g. the user can update the
		 * color shading of the hubs, the color of the contextual and the non-contextual nodes
		 */
		private int selectedRow;
		protected Color chosenColor;
		
		@Override
		public Object getCellEditorValue() {
			return this.chosenColor;
		}
	
		@Override
		public boolean isCellEditable(EventObject anEvent) {
			return true;
		}
	
		@Override
		public boolean shouldSelectCell(EventObject anEvent) {
			return true;
		}
	
		@Override
		public boolean stopCellEditing() {
			return true;
		}
	
		@Override
		public void cancelCellEditing() {
		}
	
		@Override
		public void addCellEditorListener(CellEditorListener l) {
		}
	
		@Override
		public void removeCellEditorListener(CellEditorListener l) {	
		}
		
		@Override
		public Component getTableCellEditorComponent(JTable table, Object value,
				boolean isSelected, int row, int column) {
			/**
			 * opens color chooser window and applies chosen colors to the network layout/design
			 */
			// get color as user input
			chosenColor = CyColorChooser.showDialog(this, "CHAT Color Chooser", new Color(255, 255, 10));
			this.selectedRow = row;
			if (row == 0 ){
				upperColorShade = chosenColor;
				if (degreeButton.isSelected()){
					createDegreeHubMapping();
				}else{
					createVisualStyleForContextualHubs();
				}
			}else if (row == 1){
				lowerColorShade = chosenColor;
				if (degreeButton.isSelected()){
					createDegreeHubMapping();
				}else{
					createVisualStyleForContextualHubs();
				}
			}else if (row == 2){
				contextualColor = chosenColor;
				// make visual adjustments if show contextually nodes check box is ticked
				if (highlightContextualNodesButton.isSelected()){
					createContextualityMapping();
				}
			}else if (row == 3){
				notContextualColor = chosenColor;
				// make visual adjustments if show contextually nodes check box is ticked
				if (highlightContextualNodesButton.isSelected()){
					createContextualityMapping();
				}
			}

			return this;
		}
		
		@Override
		public Component getTableCellRendererComponent(JTable table,
				Object value, boolean isSelected, boolean hasFocus, int row,
				int column) {
			/**
			 * table cell renderer
			 */
			
			this.selectedRow = row;
			if (isSelected) {
				setForeground(table.getSelectionForeground());
				setBackground(table.getSelectionBackground());
			} else {
				setForeground(table.getForeground());
				setBackground(UIManager.getColor("Button.background"));
			}

			return this;
		}

		@Override
		protected void paintComponent(Graphics g) {
			/**
			 * paints the buttons in the user-selected color
			 */
			if (this.selectedRow == 0){
				g.setColor(upperColorShade);	
			}else if (this.selectedRow == 1){
				g.setColor(lowerColorShade);
			}else if(this.selectedRow == 2){
				g.setColor(contextualColor);
			}else if(this.selectedRow == 3){
				g.setColor(notContextualColor);
			}
			
			g.fillRect(0, 0, getWidth(), getHeight());
			g.dispose();
			super.paintComponent(g);
		}
	}
	
	
}

package eu.primes.chat.internal.nodeanalyzer;

import org.cytoscape.model.CyNode;
import org.cytoscape.task.AbstractNodeViewTaskFactory;
import org.cytoscape.view.model.CyNetworkView;
import org.cytoscape.view.model.View;
import org.cytoscape.work.TaskIterator;

import eu.primes.chat.internal.CyActivator;

/**
 * This is the task factory that adds a menu to enter the Node Analyzer mode when a user
 * right-clicks on a node.
 * 
 * @author Ivan Goenawan
 */

public class NodeAnalyzerTaskFactory extends AbstractNodeViewTaskFactory{
	private CyActivator activator;
	
	
	public NodeAnalyzerTaskFactory(CyActivator activator){
		this.activator = activator;
	}
	
	
	@Override
	public TaskIterator createTaskIterator(View<CyNode> nodeView,
			CyNetworkView networkView) {
		return new TaskIterator(new NodeAnalyzerTask(nodeView, networkView, activator));
	}
	
	@Override
	public boolean isReady(View<CyNode> nodeView, CyNetworkView networkView) {
		return super.isReady(nodeView, networkView) && activator.isCHATNetwork(networkView.getModel());
	}

}

package eu.primes.chat.internal.nodeanalyzer;

import java.util.List;

import org.cytoscape.model.CyEdge;
import org.cytoscape.model.CyEdge.Type;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyNode;
import org.cytoscape.view.model.CyNetworkView;
import org.cytoscape.view.model.View;
import org.cytoscape.view.presentation.property.BasicVisualLexicon;
import org.cytoscape.work.AbstractTask;
import org.cytoscape.work.TaskMonitor;

import eu.primes.chat.internal.CyActivator;

/**
 * This task sets up the Node Analyzer mode. In the node analyzer mode, only the currently analyzed
 * node and its directly adjacent neighbours are fully visible, while the other nodes and edges are
 * made transparent, making it easy to analyze a single node.
 * 
 * @author Ivan Goenawan
 */

public class NodeAnalyzerTask extends AbstractTask{
	private View<CyNode> analyzedNodeView;
	private CyNetworkView networkView;
	private CyNetwork network;
	private CyActivator activator;
	
	
	public NodeAnalyzerTask(View<CyNode> analyzedNodeView, CyNetworkView networkView, CyActivator activator){
		this.analyzedNodeView = analyzedNodeView;
		this.networkView = networkView;
		this.network = networkView.getModel();
		this.activator = activator;
	}
	
	@Override
	public void run(TaskMonitor taskMonitor) throws Exception {
		
		for (View<CyEdge> edgeView : networkView.getEdgeViews()){
			edgeView.setLockedValue(BasicVisualLexicon.EDGE_VISIBLE, false);
		}
		
		for (View<CyNode> nodeView : networkView.getNodeViews()){
			nodeView.setLockedValue(BasicVisualLexicon.NODE_VISIBLE, false);
		}
		
		
		List<CyNode> nodes = network.getNeighborList(analyzedNodeView.getModel(), Type.ANY);
		nodes.add(analyzedNodeView.getModel());  //'nodes' now contains both the current node and its neighbours
		List<CyEdge> adjacentEdges = network.getAdjacentEdgeList(analyzedNodeView.getModel(), Type.ANY);
		
		
		for (CyNode node : nodes){
			View<CyNode> nodeView = networkView.getNodeView(node);
			nodeView.clearValueLock(BasicVisualLexicon.NODE_VISIBLE);
		}
		
		for (CyEdge adjacentEdge : adjacentEdges){		
			View<CyEdge> adjacentEdgeView = networkView.getEdgeView(adjacentEdge);
			adjacentEdgeView.clearValueLock(BasicVisualLexicon.EDGE_VISIBLE);
		}
		
		networkView.updateView();
		activator.nodeAnalyzerActivated(network, analyzedNodeView.getModel());
	}	
}

package eu.primes.chat.internal;

import java.awt.AWTException;
import java.awt.Dimension;
import java.awt.Robot;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowFocusListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;
import java.util.Set;

import javax.swing.JOptionPane;

import org.cytoscape.app.swing.CySwingAppAdapter;
import org.cytoscape.application.swing.CytoPanel;
import org.cytoscape.application.swing.CytoPanelComponent;
import org.cytoscape.application.swing.CytoPanelName;
import org.cytoscape.application.swing.CytoPanelState;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyNode;
import org.cytoscape.model.events.NetworkAboutToBeDestroyedEvent;
import org.cytoscape.model.events.NetworkAboutToBeDestroyedListener;
import org.cytoscape.service.util.AbstractCyActivator;
import org.cytoscape.task.NodeViewTaskFactory;
import org.cytoscape.view.model.CyNetworkView;
import org.cytoscape.view.model.events.NetworkViewAboutToBeDestroyedEvent;
import org.cytoscape.view.model.events.NetworkViewAboutToBeDestroyedListener;
import org.cytoscape.work.TaskFactory;
import org.osgi.framework.BundleContext;

import eu.primes.chat.internal.nodeanalyzer.NodeAnalyzerTaskFactory;

/**
 * The CyActivator starts this app/bundle, imports services and establishes communication with the
 * OSGi environment.
 * 
 * @author Tanja Muetze, Ivan Goenawan
 */

public class CyActivator extends AbstractCyActivator implements NetworkViewAboutToBeDestroyedListener, NetworkAboutToBeDestroyedListener {

	private HashMap<CyNetwork, ResultPanel> activeNetworks;
	private CySwingAppAdapter appAdapter;
	private CyNetwork lastDeleted;
	
	@Override
	public void start(BundleContext context) throws Exception {
		appAdapter = getService(context, CySwingAppAdapter.class);
		activeNetworks = new HashMap<CyNetwork, ResultPanel>();
		
		ShowInitDialogTaskFactory showInitDialogTaskFactory = new ShowInitDialogTaskFactory(appAdapter, this);
		Properties properties = new Properties();
		properties.put("title", "CHAT");
		properties.put("preferredMenu", "Apps");
		
		NodeAnalyzerTaskFactory nodeAnalyzerTaskFactory = new NodeAnalyzerTaskFactory(this);
		Properties nodeAnalyzerTaskProperty = new Properties();
		nodeAnalyzerTaskProperty.setProperty("title", "CHAT Node Analyzer");
		
		registerService(context, showInitDialogTaskFactory, TaskFactory.class, properties);
		registerService(context, this, NetworkAboutToBeDestroyedListener.class, new Properties());
		registerService(context, this, NetworkViewAboutToBeDestroyedListener.class, new Properties());
		registerService(context, nodeAnalyzerTaskFactory, NodeViewTaskFactory.class, nodeAnalyzerTaskProperty);
	}

	
	public void constructNetworkFinish(final CyNetwork network, final CyNetworkView networkView, String[] userDefinedParameters, ArrayList<String> interactionTypesNamesList){
		//register network and network view
		appAdapter.getCyNetworkManager().addNetwork(network);
		appAdapter.getCyNetworkViewManager().addNetworkView(networkView);
		networkView.fitContent();
		ResultPanel resultPanel = new ResultPanel(appAdapter, network, network.toString(), userDefinedParameters, interactionTypesNamesList);
		
		resultPanel.setSize(new Dimension(650, 600));
		resultPanel.doLayout();
		
		activeNetworks.put(network, resultPanel);
		appAdapter.getCyServiceRegistrar().registerService(resultPanel, CytoPanelComponent.class, new Properties());
		
		int index = appAdapter.getCySwingApplication().getCytoPanel(CytoPanelName.EAST).indexOfComponent(resultPanel);
		appAdapter.getCySwingApplication().getCytoPanel(CytoPanelName.EAST).setSelectedIndex(index);
		appAdapter.getCySwingApplication().getCytoPanel(CytoPanelName.EAST).setState(CytoPanelState.FLOAT);
		
		//activate "show graphics details". would this work on other OSes?
		appAdapter.getCySwingApplication().getJFrame().addWindowFocusListener(new WindowFocusListener() {
			@Override
			public void windowLostFocus(WindowEvent event) {
			}
			
			@Override
			public void windowGainedFocus(WindowEvent event) {
				try {
					if (network.getEdgeCount() <= 5000){  //if network is large, don't activate show graphics details
						Robot robot = new Robot();
						robot.mouseRelease(InputEvent.BUTTON1_DOWN_MASK);   //because if mouse is clicked, keyboard shortcut doesn't work
						appAdapter.getCyApplicationManager().setCurrentNetworkView(networkView);
						
						robot.keyPress(KeyEvent.VK_CONTROL);
						robot.keyPress(KeyEvent.VK_SHIFT);
						robot.keyPress(KeyEvent.VK_F);
						
						robot.keyRelease(KeyEvent.VK_F);
						robot.keyRelease(KeyEvent.VK_SHIFT);
						robot.keyRelease(KeyEvent.VK_CONTROL);
					}
				} catch (AWTException e) {
					e.printStackTrace();
				}
				appAdapter.getCySwingApplication().getJFrame().removeWindowFocusListener(this);
			}
		});
	}
	
	public boolean isCHATNetwork(CyNetwork network){
		return activeNetworks.containsKey(network);
	}
	
	public void nodeAnalyzerActivated(CyNetwork network, CyNode node){
		ResultPanel resultPanel = activeNetworks.get(network);
		resultPanel.disableSlider();
		NodeAnalyzerPanel nodeAnalyzerPanel = resultPanel.getNodeAnalyzerPanel();
		nodeAnalyzerPanel.setVisible(true);
		nodeAnalyzerPanel.nodeAnalyzerActivated(node);
		
		CytoPanel east = appAdapter.getCySwingApplication().getCytoPanel(CytoPanelName.EAST);
		east.setSelectedIndex(east.indexOfComponent(resultPanel));
		if (east.getState() == CytoPanelState.HIDE) east.setState(CytoPanelState.FLOAT);
	}

	@Override
	public void handleEvent(NetworkAboutToBeDestroyedEvent e) {
		/**
		 * Destroys network, network view and result panel view if network is destroyed.
		 */
		CyNetwork network = e.getNetwork();
		if (!activeNetworks.containsKey(network)) return;
		lastDeleted = network;
		
		ResultPanel panel = activeNetworks.get(network);
		appAdapter.getCyServiceRegistrar().unregisterService(panel, CytoPanelComponent.class);
		activeNetworks.remove(network);
	}
	
	@Override
	public void handleEvent(NetworkViewAboutToBeDestroyedEvent e) {
		/**
		 * Destroys network, network view and result panel view if network view is destroyed.
		 */
		CyNetworkView networkView = e.getNetworkView();
		CyNetwork network = networkView.getModel();
		if (!activeNetworks.containsKey(network)) return;
		if (network == lastDeleted) return;
		
		int response = JOptionPane.showConfirmDialog(appAdapter.getCySwingApplication().getJFrame(), 
				"Also destroy network and close CHAT's result panel?", "CHAT", JOptionPane.YES_NO_OPTION);
		
		if (response == JOptionPane.YES_OPTION){
			appAdapter.getCyNetworkManager().destroyNetwork(network);
		}
	}
}

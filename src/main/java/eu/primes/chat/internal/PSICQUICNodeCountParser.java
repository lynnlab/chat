package eu.primes.chat.internal;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.zip.GZIPInputStream;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Downloads the pre-computed node counts from server
 * 
 * @author Ivan Goenawan
 */

public class PSICQUICNodeCountParser {
	
	private static final String ADDRESS = "http://webpub.primesdb.org/chat_psicquic_data/PSICQUICNodeCount.json.gz";
	
	private static final String SPECIES_ID_MAPPING = "species_id_mapping";
	private static final String INTERACTION_TYPE_MAPPING = "interaction_type_mapping";
	
	private static final String DATABASES = "databases";
	private static final String AVAILABLE_INTERACTION_TYPES = "available_interaction_types";
	private static final String AVAILABLE_ID_TYPES = "available_id_types";
	private static final String SPECIES_LIST = "species_list";
	
	private static JSONObject psicquicJSON; //need to synchronize if READ from many threads?
	private static long lastDownloadTime;
	
	
	
	public static int getNodeCount(String database, String species, String idType, Set<String> interactionTypes) throws Exception{
		if (database == null) throw new Exception("A database must be selected.");
		if (species == null) throw new Exception("A species must be selected.");
		if (idType == null) throw new Exception("An ID type must be selected.");
		
		
		JSONObject psicquicJSON = loadJSON();
		JSONObject databaseJSON = psicquicJSON.getJSONObject(DATABASES).getJSONObject(database);
		JSONObject speciesJSON = databaseJSON.getJSONObject(SPECIES_LIST).getJSONObject(species);
		JSONArray idTypeJSON;
		try{
			 idTypeJSON = speciesJSON.getJSONArray(idType);  //this contains the list of interactors
		}catch(Exception e){
			throw new Exception("ID type (" + idType + ") is not available for the selected species." );
		}
		
		
		if (interactionTypes == null || interactionTypes.isEmpty()){
			return idTypeJSON.length();
		}else{
			int nodeCount = 0;
			for (int i = 0; i < idTypeJSON.length(); i++){
				JSONArray nodeJSON = idTypeJSON.getJSONArray(i);   //each interactor is represented as a list of interaction types
				
				boolean match = false;  //indicating matching interaction type
				for (int j = 0; j < nodeJSON.length(); j++){
					if (interactionTypes.contains(nodeJSON.getString(j))){
						match = true;
						break;
					}
				}
				
				if (match) nodeCount++;
			}
			return nodeCount;
		}
	}
	
	
	
	//return available species IDs ordered by the number of interactors
	//the number of interactors of each species is the max among all ID types
	public static Map<String, List<String>> getAvailableSpecies() throws Exception{
		JSONObject psicquicJSON = loadJSON();
		JSONObject databasesJSON = psicquicJSON.getJSONObject(DATABASES);
		
		HashMap<String, List<String>> databaseToAvailableSpecies = new HashMap<String, List<String>>();
		for (String database : databasesJSON.keySet()){
			final JSONObject speciesListJSON = databasesJSON.getJSONObject(database).getJSONObject(SPECIES_LIST);
			ArrayList<String> availableSpecies = new ArrayList<String>(speciesListJSON.keySet());
			
			Collections.sort(availableSpecies, new Comparator<String>() {
				@Override
				public int compare(String speciesA, String speciesB) {
					JSONObject speciesAJSON = speciesListJSON.getJSONObject(speciesA);
					JSONObject speciesBJSON = speciesListJSON.getJSONObject(speciesB);
					
					int maxA = 0;
					for (String idType : speciesAJSON.keySet()){
						JSONArray idTypeJSON = speciesAJSON.getJSONArray(idType);
						if (idTypeJSON.length() > maxA) maxA = idTypeJSON.length();
					}
					
					int maxB = 0;
					for (String idType : speciesBJSON.keySet()){
						JSONArray idTypeJSON = speciesBJSON.getJSONArray(idType);
						if (idTypeJSON.length() > maxB) maxB = idTypeJSON.length();
					}
					
					return maxB - maxA;  //descending order
				}
			});
			
			databaseToAvailableSpecies.put(database, availableSpecies);
		}
		
		return databaseToAvailableSpecies;
	}
	
	public static Map<String, List<String>> getAvailableIdTypes() throws Exception{
		JSONObject psicquicJSON = loadJSON();
		JSONObject databasesJSON = psicquicJSON.getJSONObject(DATABASES);
		
		HashMap<String, List<String>> databaseToAvailableIdType = new HashMap<String, List<String>>();
		for (String database : databasesJSON.keySet()){
			JSONArray availableIdTypesJSON = databasesJSON.getJSONObject(database).getJSONArray(AVAILABLE_ID_TYPES);
			
			ArrayList<String> availableIdTypes = new ArrayList<String>();
			for (int i = 0; i < availableIdTypesJSON.length(); i++){
				availableIdTypes.add(availableIdTypesJSON.getString(i));
			}
			
			databaseToAvailableIdType.put(database, availableIdTypes);
		}
		
		return databaseToAvailableIdType;
	}
	
	public static Map<String, List<String>> getAvailableInteractionTypes() throws Exception{
		JSONObject psicquicJSON = loadJSON();
		JSONObject databasesJSON = psicquicJSON.getJSONObject(DATABASES);
		
		HashMap<String, List<String>> databaseToAvailableInterType = new HashMap<String, List<String>>();
		for (String database : databasesJSON.keySet()){
			JSONArray availableInterTypesJSON = databasesJSON.getJSONObject(database).getJSONArray(AVAILABLE_INTERACTION_TYPES);
			
			ArrayList<String> availableInterTypes = new ArrayList<String>();
			for (int i = 0; i < availableInterTypesJSON.length(); i++){
				availableInterTypes.add(availableInterTypesJSON.getString(i));
			}
			
			databaseToAvailableInterType.put(database, availableInterTypes);
		}
		
		return databaseToAvailableInterType;
	}
	
	public static Map<String, String> getSpeciesIdMapping() throws Exception{
		JSONObject psicquicJSON = loadJSON();
		JSONObject speciesIdMappingJSON = psicquicJSON.getJSONObject(SPECIES_ID_MAPPING);
	
		Map<String, String> speciesIdMapping = new HashMap<String, String>();
		
		for (String speciesId : speciesIdMappingJSON.keySet()){
			speciesIdMapping.put(speciesId, speciesIdMappingJSON.getString(speciesId));
		}
		
		return speciesIdMapping;
	}
	
	public static Map<String, String> getInteractionTypeIdMapping() throws Exception{
		JSONObject psicquicJSON = loadJSON();
		JSONObject interactionTypeIdMappingJSON = psicquicJSON.getJSONObject(INTERACTION_TYPE_MAPPING);
		
		Map<String, String> interactionTypeIdMapping = new HashMap<String, String>();
		
		for (String id : interactionTypeIdMappingJSON.keySet()){
			interactionTypeIdMapping.put(id, interactionTypeIdMappingJSON.getString(id));
		}
		
		return interactionTypeIdMapping;
	}
	
	
	
	private static JSONObject loadJSON() throws Exception{
		if (psicquicJSON == null || 
				System.currentTimeMillis() - lastDownloadTime > 3600000){
			
			try{
				InputStream input = new URL(ADDRESS).openStream();
				GZIPInputStream gzipInput = new GZIPInputStream(input);
				BufferedReader reader = new BufferedReader(new InputStreamReader(gzipInput));
				psicquicJSON = new JSONObject(reader.readLine());
				reader.close();
				lastDownloadTime = System.currentTimeMillis();
			}catch(Exception e){
				if (psicquicJSON == null) throw new Exception("Cannot connect to CHAT's server: " + e.getMessage());
			}
			
		}
		
		return psicquicJSON;
	}
}

package eu.primes.chat.internal;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.Thread.State;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.zip.GZIPInputStream;

import org.apache.commons.math3.distribution.HypergeometricDistribution;
import org.cytoscape.app.swing.CySwingAppAdapter;
import org.cytoscape.model.CyEdge;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyNode;
import org.cytoscape.model.CyRow;
import org.cytoscape.model.CyTable;
import org.cytoscape.view.layout.CyLayoutAlgorithm;
import org.cytoscape.work.AbstractTask;
import org.cytoscape.work.TaskMonitor;

/**
 * This builds the network based on the user-provided contextual genes, the chosen database,
 * ID types and interaction types. It calculates the p-value for over-representation using a
 * hypergeometric test and corrects it by the Benjamini-Hochberg method for multiple testing. 
 * 
 * @author Ivan Goenawan, Tanja Muetze
 */

public class ConstructNetworkTask extends AbstractTask {

	private CySwingAppAdapter appAdapter;
	private CyActivator activator;
	
	private Set<String> identifierSet;  //same as identifierList, but all guaranteed lower case
	private List<String> identifierList;
	private List<String> attributeList;
	private List<Boolean> importanceList;
	private boolean numeric;
	
	private String databaseUrl;
	private String databaseName;
	private String species;
	private String idType;
	private String[] userDefinedParameters;
	private Set<String> interactionTypes;
	private ArrayList<String> interactionTypesNamesList;
	
	
	private HashMap<Node, HashMap<Node, Edge>> matrix;
	private HashMap<String, Node> uniqueIdToNodeMap;        //should we make this case insensitive?
	private HashMap<String, Set<String>> idToUniqueIdsMap;  //one id could potentially map to more than one unique ids
	
	private int totalNodeCountN;
	private int contextualNodeCountK;
	
	private String networkName;
	private CyLayoutAlgorithm layout;
	
	
	private static final int retryLimit = 3;                //retry limit per each download thread
	private static final int idPerRequest = 20;				//the number of ids included in each request
	private static final int activeThreadsLimit = 30;       //the number of download threads that can be active simultaneously
	
	
	public static final String NODE_DISPLAY_NAME = "Display Name";
	public static final String NODE_SUPPLIED_ID = "Supplied ID";
	public static final String NODE_ALTERNATIVE_IDS = "Alternative IDs";
	public static final String NODE_ALIASES = "Aliases";
	public static final String NODE_NCBI_TAXONOMY = "NCBI Taxonomy";
	public static final String NODE_CONTEXT_ATTRIBUTE = "Context Attribute";
	public static final String NODE_CONTEXTUALLY_IMPORTANT = "Contextually Important";
	public static final String NODE_TOTAL_NEIGHBOURS = "Total Neighbours in DB";
	public static final String NODE_CONTEXTUAL_NEIGHBOURS = "Contextual Neighbours";
	public static final String NODE_PVALUE = "P-Value";
	public static final String NODE_LOG_PVALUE = "-log10PV";
	public static final String NODE_ADJUSTED_PVALUE = "Adjusted P-Value";
	
	
	public static final String EDGE_DETECTION_METHOD = "Detection Method";
	public static final String EDGE_CONFIDENCE_SCORE = "Confidence Score";
	public static final String EDGE_FIRST_AUTHOR = "First Author";
	public static final String EDGE_PUBLICATION_ID = "Publication ID";
	public static final String EDGE_SOURCE_DATABASE = "Source Database";
	
	public static final String NETWORK_CONTEXTUAL_NODES = "Contextual Nodes";
	public static final String NETWORK_UNCONTEXTUAL_NODES = "Uncontextual Nodes";
	public static final String NETWORK_TOTAL_NODES = "Total Nodes";
	
	
	public ConstructNetworkTask(CySwingAppAdapter appAdapter, CyActivator activator,
								List<String> identifiers, List<String> attributes, List<Boolean> importance,
								boolean numeric, String databaseUrl, String databaseName, String species, String idType,
								Set<String> interactionTypes, int totalNodes, String networkName, CyLayoutAlgorithm layout, ArrayList<String> interactionTypesNamesList){
		this.appAdapter = appAdapter;
		this.activator = activator;
		
		this.identifierList = identifiers;
		this.attributeList = attributes;
		this.importanceList = importance;
		this.numeric = numeric;
		this.databaseUrl = databaseUrl;
		this.databaseName = databaseName;
		this.species = species;
		this.idType = idType.toLowerCase();
		this.interactionTypes = interactionTypes;
		this.interactionTypesNamesList = interactionTypesNamesList;
		this.networkName = networkName;
		this.layout = layout;
		
		
		identifierSet = new HashSet<String>();
		for (String id : identifierList){
			identifierSet.add(id.toLowerCase());
		}
		
		uniqueIdToNodeMap = new HashMap<String, Node>();
		idToUniqueIdsMap = new HashMap<String, Set<String>>();
		matrix = new HashMap<Node, HashMap<Node, Edge>>();
		
		
		totalNodeCountN = totalNodes;  //only those that have at least 1 matching interaction type
		contextualNodeCountK = 0;      //only those that have at least 1 matching interaction type
		
		userDefinedParameters = new String[]{databaseName, species, idType};
	}
	
	@Override
	public void run(TaskMonitor taskMonitor) throws Exception {
		taskMonitor.setTitle("CHAT");
		
		//---------------------------------------------------------------------------------------------------------
		//First fetch with nodes in the user's list
		//---------------------------------------------------------------------------------------------------------
		
		taskMonitor.setStatusMessage("Fetching interactions for user-supplied nodes.");
		Set<String> firstFetch = fetchPSICQUICMultiThreaded(identifierList, taskMonitor);
		
		taskMonitor.setProgress(-1);
		taskMonitor.setStatusMessage("Parsing interactions...");
		if (firstFetch.isEmpty()) throw new Exception ("No interactions found. Please ensure you supplied identifiers that exist in the database. It is also possible that CHAT was unable to connect to PSICQUIC, for example if the selected database is down at the moment. Please retry your query with different identifiers and/or attributes or a different database or species.");
		
		for (String mitabLine : firstFetch){
			MitabEntry entry = new MitabEntry(mitabLine);
			if (interactionTypes != null && !interactionTypes.isEmpty() && 
					!interactionTypes.contains(entry.getInteractionTypeId())) continue;
			
			//Do we need to double check the species (tax ID) here?? We already limit the PSICQUIC request...
			
			
			//ignore self-interactions
			if (entry.getUniqueIdA().indexOf(':') == -1) continue;
			if (entry.getUniqueIdB().indexOf(':') == -1) continue;
			if (entry.getUniqueIdA().equals(entry.getUniqueIdB())) continue;
			
			
			
			Node nodeA = uniqueIdToNodeMap.get(entry.getUniqueIdA());
			if (nodeA == null){
				nodeA = new Node(entry.getUniqueIdA(), entry.getAlternativeIdA(), entry.getAliasesA(), entry.getNcbiTaxIdA(), idType);
				if (nodeA.getIdList().isEmpty()){
					//ignore interaction if node doesn't have ID of the chosen type
					continue;
				}
			}
			
			Node nodeB = uniqueIdToNodeMap.get(entry.getUniqueIdB());
			if (nodeB == null){
				nodeB = new Node(entry.getUniqueIdB(), entry.getAlternativeIdB(), entry.getAliasesB(), entry.getNcbiTaxIdB(), idType);
				if (nodeB.getIdList().isEmpty()){
					//ignore interaction if node doesn't have ID of the chosen type
					continue;
				}
			}
			
			
			//Make sure that at least one of the nodes is in our list
			//because the PSICQUIC query is not ID-type specific
			boolean nodeAInUserList = false;
			for (String id : nodeA.getIdList()){
				if (identifierSet.contains(id)){
					nodeAInUserList = true;
					break;
				}
			}
			
			boolean nodeBInUserList = false;
			for (String id : nodeB.getIdList()){
				if (identifierSet.contains(id)){
					nodeBInUserList = true;
					break;
				}
			}
			
			if (!nodeAInUserList && !nodeBInUserList) continue;
			
			
			
			
			if (!uniqueIdToNodeMap.containsKey(entry.getUniqueIdA())){
				uniqueIdToNodeMap.put(entry.getUniqueIdA(), nodeA);
				matrix.put(nodeA, new HashMap<Node, Edge>());
			}
			
			if (!uniqueIdToNodeMap.containsKey(entry.getUniqueIdB())){
				uniqueIdToNodeMap.put(entry.getUniqueIdB(), nodeB);
				matrix.put(nodeB, new HashMap<Node, Edge>());
			}
			
			
			
			//create mapping from id to unique IDs (for identifying contextual nodes)
			for (String id : nodeA.getIdList()){
				if (!idToUniqueIdsMap.containsKey(id)){
					idToUniqueIdsMap.put(id, new HashSet<String>());
				}
				idToUniqueIdsMap.get(id).add(entry.getUniqueIdA());
			}
			
			for (String id : nodeB.getIdList()){
				if (!idToUniqueIdsMap.containsKey(id)){
					idToUniqueIdsMap.put(id, new HashSet<String>());
				}
				idToUniqueIdsMap.get(id).add(entry.getUniqueIdB());
			}
			
			
			
			// create new edges while avoiding duplicate edges
			Edge edge = matrix.get(nodeA).get(nodeB);
			if (edge == null) edge = matrix.get(nodeB).get(nodeA);   //remember one edge is not repeated twice in the matrix
			if (edge == null){
				edge = new Edge(entry.getInteractionId(), entry.getInteractionType(), entry.getDetectionMethod(), 
						entry.getConfidenceScore(), entry.getFirstAuthor(), entry.getPublicationId(), entry.getSourceDatabases());
				matrix.get(nodeA).put(nodeB, edge);
			}
		}
		
		if (uniqueIdToNodeMap.isEmpty()){
			throw new Exception ("No interactions found with the provided interaction types for the provided genes. Please choose different interaction types or expand your selection.");
		}
		
		
		
		//-----------------------------------------------------------------------------------------------------
		//Mark nodes that are contextual
		//-----------------------------------------------------------------------------------------------------
		HashSet<String> uniqueIdsinUserList = new HashSet<String>();
		for (int i = 0; i < identifierList.size(); i++){
			String suppliedId = identifierList.get(i).trim();
			Set<String> uniqueIds = idToUniqueIdsMap.get(suppliedId.toLowerCase());
			
			if (uniqueIds != null){
				for (String uniqueId : uniqueIds){
					uniqueIdsinUserList.add(uniqueId);
					Node node = uniqueIdToNodeMap.get(uniqueId);
					if (!node.isContextual()){          //a node if contextual if at least one of its IDs is considered contextual
						node.setSuppliedId(suppliedId);
						node.setContextAttribute(attributeList.get(i));
						node.setContextual(importanceList.get(i));
					}
				}
			}
		}
		
		
		//-----------------------------------------------------------------------------------------------------
		//Second fetch with first neighbours (direct interactors) of nodes in user's list
		//-----------------------------------------------------------------------------------------------------
		
		HashSet<String> uniqueIdsNotInUserList = new HashSet<String>(uniqueIdToNodeMap.keySet());
		uniqueIdsNotInUserList.removeAll(uniqueIdsinUserList);
		
		ArrayList<String> secondQueryIds = new ArrayList<String>();
		for (String id : uniqueIdsNotInUserList){
			secondQueryIds.add(extractValidId(id));
		}
		
		
		taskMonitor.setStatusMessage("Fetching the interactors of the first neighbour interactors");
		Set<String> secondFetch = fetchPSICQUICMultiThreaded(secondQueryIds, taskMonitor);
		
		taskMonitor.setProgress(-1);
		taskMonitor.setStatusMessage("Parsing interactions...");
		
		
		//key=existing node in the network, values=unique id of invisible neighbours.
		HashMap<Node, HashSet<String>> invisibleNeighbours = new HashMap<Node, HashSet<String>>();
		
		
		for (String mitabLine : secondFetch){
			MitabEntry entry = new MitabEntry(mitabLine);
			if (interactionTypes != null && !interactionTypes.isEmpty() && 
					!interactionTypes.contains(entry.getInteractionTypeId())) continue;
			
			
			//ignore self-interactions
			if (entry.getUniqueIdA().indexOf(':') == -1) continue;
			if (entry.getUniqueIdB().indexOf(':') == -1) continue;
			if (entry.getUniqueIdA().equals(entry.getUniqueIdB())) continue;
			
						
			Node nodeA = uniqueIdToNodeMap.get(entry.getUniqueIdA());
			Node nodeB = uniqueIdToNodeMap.get(entry.getUniqueIdB());

			
			if (nodeA == null && nodeB == null) {
				continue;
			
			}else if (nodeA != null && nodeB != null){       //if both nodes are direct interactors, then include their interaction
				
				Edge edge = matrix.get(nodeA).get(nodeB);
				if (edge == null) edge = matrix.get(nodeB).get(nodeA);
				if (edge == null){
					edge = new Edge(entry.getInteractionId(), entry.getInteractionType(), entry.getDetectionMethod(), 
							entry.getConfidenceScore(), entry.getFirstAuthor(), entry.getPublicationId(), entry.getSourceDatabases());
					matrix.get(nodeA).put(nodeB, edge);
				}
			
			}else{	//if one node is not a direct interactor, then just increment the neighbour count
				
				String newNodeCombinedIds, newNodeUniqueId;
				Node existingNode;
				
				if (nodeA == null){
					newNodeCombinedIds = (entry.getUniqueIdA() + "|" + entry.getAlternativeIdA() + "|" + entry.getAliasesA()).toLowerCase();
					newNodeUniqueId = entry.getUniqueIdA();
					existingNode = nodeB;
				}else{
					newNodeCombinedIds = (entry.getUniqueIdB() + "|" + entry.getAlternativeIdB() + "|" + entry.getAliasesB()).toLowerCase();
					newNodeUniqueId = entry.getUniqueIdB();
					existingNode = nodeA;
				}
				
				
				//check if new node has the right id type
				if (!newNodeCombinedIds.contains(idType + ":")) continue;
					
				
				
				if (!invisibleNeighbours.containsKey(existingNode)){
					invisibleNeighbours.put(existingNode, new HashSet<String>());
				}
				
				if (!invisibleNeighbours.get(existingNode).contains(newNodeUniqueId)){
					existingNode.incrementNeighbour(false);
					invisibleNeighbours.get(existingNode).add(newNodeUniqueId);
				}
			}
		}
		
		
		//-----------------------------------------------------------------------------------------------------
		//Build CyNetwork + count the number of neighbours
		//-----------------------------------------------------------------------------------------------------
		taskMonitor.setStatusMessage("Building network.");
		
		HashMap<Node, CyNode> nodeToCyNodeMap = new HashMap<Node,CyNode>();
		CyNetwork network = appAdapter.getCyNetworkFactory().createNetwork();
		network.getRow(network).set(CyNetwork.NAME, networkName);
		
		// create node, edge and network tables
		CyTable nodeTable = network.getDefaultNodeTable();
		nodeTable.createColumn(NODE_DISPLAY_NAME, String.class, false);
		nodeTable.createColumn(NODE_SUPPLIED_ID, String.class, false);
		nodeTable.createColumn(NODE_ALTERNATIVE_IDS, String.class, false);
		nodeTable.createColumn(NODE_ALIASES, String.class, false);
		nodeTable.createColumn(NODE_NCBI_TAXONOMY, String.class, false);
		nodeTable.createColumn(NODE_CONTEXT_ATTRIBUTE, numeric?Double.class:String.class, false);
		nodeTable.createColumn(NODE_CONTEXTUALLY_IMPORTANT, Boolean.class, false);
		nodeTable.createColumn(NODE_TOTAL_NEIGHBOURS, Integer.class, false);
		nodeTable.createColumn(NODE_CONTEXTUAL_NEIGHBOURS, Integer.class, false);
		nodeTable.createColumn(NODE_PVALUE, Double.class, false);
		nodeTable.createColumn(NODE_LOG_PVALUE, Double.class, false);
		nodeTable.createColumn(NODE_ADJUSTED_PVALUE, Double.class, false);
		
		CyTable edgeTable = network.getDefaultEdgeTable();
		edgeTable.createColumn(EDGE_DETECTION_METHOD, String.class, false);
		edgeTable.createColumn(EDGE_CONFIDENCE_SCORE, String.class, false);
		edgeTable.createColumn(EDGE_FIRST_AUTHOR, String.class, false);
		edgeTable.createColumn(EDGE_PUBLICATION_ID, String.class, false);
		edgeTable.createColumn(EDGE_SOURCE_DATABASE, String.class, false);
		
		CyTable networkTable = network.getDefaultNetworkTable();
		networkTable.createColumn(NETWORK_CONTEXTUAL_NODES, Integer.class, false);
		networkTable.createColumn(NETWORK_UNCONTEXTUAL_NODES, Integer.class, false);
		networkTable.createColumn(NETWORK_TOTAL_NODES, Integer.class, false);
		
		// add nodes to network and add node data to node table
		for (Node nodeModel : uniqueIdToNodeMap.values()){
			CyNode node = network.addNode();
			nodeToCyNodeMap.put(nodeModel, node);
			CyRow row = network.getRow(node);
			row.set(CyNetwork.NAME, nodeModel.getUniqueId());
			row.set(NODE_DISPLAY_NAME, nodeModel.getDisplayName());
			row.set(NODE_SUPPLIED_ID, nodeModel.getSuppliedId());
			row.set(NODE_ALTERNATIVE_IDS, nodeModel.getAlternativeId());
			row.set(NODE_ALIASES, nodeModel.getAliases());
			row.set(NODE_NCBI_TAXONOMY, nodeModel.getNCBITaxId());
			row.set(NODE_CONTEXT_ATTRIBUTE, (numeric && nodeModel.getContextAttribute() != null)?
					Double.parseDouble(nodeModel.getContextAttribute()):nodeModel.getContextAttribute());
			row.set(NODE_CONTEXTUALLY_IMPORTANT, nodeModel.isContextual());
			
			//increment total/global count of contextual nodes in the network
			if (nodeModel.isContextual()) contextualNodeCountK++;
		}
		
		// add edge to network and add edge data do edge table
		for (Node nodeA : matrix.keySet()){
			for (Node nodeB : matrix.get(nodeA).keySet()){
				Edge edgeModel = matrix.get(nodeA).get(nodeB);
				
				//increment node count of contextual neighbors on a per node basis
				nodeA.incrementNeighbour(nodeB.isContextual());
				nodeB.incrementNeighbour(nodeA.isContextual());
				
				
				//don't show edges between first-neighbours (only show edges of nodes in user's list)
				if (!uniqueIdsinUserList.contains(nodeA.getUniqueId()) &&
						!uniqueIdsinUserList.contains(nodeB.getUniqueId())){
					continue;
				}
				
				
				//we need to make the edges directed to prevent this bug http://code.cytoscape.org/redmine/issues/3255
				CyEdge edge = network.addEdge(nodeToCyNodeMap.get(nodeA), nodeToCyNodeMap.get(nodeB), true);
				CyRow row = network.getRow(edge);
				row.set(CyNetwork.NAME, edgeModel.getInteractionId());
				row.set(CyEdge.INTERACTION, edgeModel.getInteractionType());
				row.set(EDGE_DETECTION_METHOD, edgeModel.getDetectionMethod());
				row.set(EDGE_CONFIDENCE_SCORE, edgeModel.getConfidenceScore());
				row.set(EDGE_FIRST_AUTHOR, edgeModel.getFirstAuthor());
				row.set(EDGE_PUBLICATION_ID, edgeModel.getPublicationId());
				row.set(EDGE_SOURCE_DATABASE, edgeModel.getSourceDatabases());
			}
		}
		
		// set network parameters of global node count, contextual node count and non-contextual node count
		CyRow networkRow = network.getRow(network);
		networkRow.set(NETWORK_CONTEXTUAL_NODES, contextualNodeCountK);
		networkRow.set(NETWORK_UNCONTEXTUAL_NODES, totalNodeCountN - contextualNodeCountK);
		networkRow.set(NETWORK_TOTAL_NODES, totalNodeCountN);
		
		
		//-------------------------------------------------------------------------------------------------------
		//Hypergeometric calculation
		//-------------------------------------------------------------------------------------------------------
		
		for (Node nodeModel : nodeToCyNodeMap.keySet()){
			int numNeighbours = nodeModel.getNumNeighbours();
			int numContextualNeighbours = nodeModel.getNumContextualNeighbours();
			
			int potentialNeighbours = totalNodeCountN;
			int potentialContextualNeighbours = contextualNodeCountK;
			
			//calculate the upper-cumulative probability to find p-value for over-representation
			double pValue = new HypergeometricDistribution(potentialNeighbours, potentialContextualNeighbours, numNeighbours).upperCumulativeProbability(numContextualNeighbours);
			nodeModel.setPValue(pValue);
		}
		
		//-------------------------------------------------------------------------------------------------------
		//Benjamini-Hochberg correction
		//-------------------------------------------------------------------------------------------------------
		
		ArrayList<Node> nodes = new ArrayList<Node>(nodeToCyNodeMap.keySet());
		Collections.sort(nodes, new Comparator<Node>(){
			@Override
			public int compare(Node nodeA, Node nodeB) {
				if (nodeA.getPValue() > nodeB.getPValue()) return 1;
				else if (nodeA.getPValue() < nodeB.getPValue()) return -1;
				else return 0;
			}
		});
		
		Node previousNode = nodes.get(nodes.size() - 1);
		previousNode.setAdjustedPValue(previousNode.getPValue());
		
		for (int i = nodes.size() - 2; i >= 0; i--){
			Node node = nodes.get(i);
			double corrected = node.getPValue() * ((double)nodes.size() / (i + 1));
			node.setAdjustedPValue(corrected < previousNode.getAdjustedPValue() ? corrected : previousNode.getAdjustedPValue());
			previousNode = node;
		}
		
		
		
		for (Node nodeModel : nodeToCyNodeMap.keySet()){
			CyNode node = nodeToCyNodeMap.get(nodeModel);
			CyRow row = network.getRow(node);
			row.set(NODE_TOTAL_NEIGHBOURS, nodeModel.getNumNeighbours());
			row.set(NODE_CONTEXTUAL_NEIGHBOURS, nodeModel.getNumContextualNeighbours());
			row.set(NODE_PVALUE, nodeModel.getPValue());
			row.set(NODE_LOG_PVALUE, -1 * Math.log10(nodeModel.getAdjustedPValue()));
			row.set(NODE_ADJUSTED_PVALUE, nodeModel.getAdjustedPValue());
		}
		
		//------------------------------------------------------------------------------------------------------
		//Construct network view
		//------------------------------------------------------------------------------------------------------
		insertTasksAfterCurrentTask(new ConstructNetworkViewTask(appAdapter, activator, network, userDefinedParameters, interactionTypesNamesList, layout));
	}


	private Set<String> fetchPSICQUICMultiThreaded(List<String> ids, TaskMonitor taskMonitor) throws Exception{
		/**
		 * Fetches interactions for gene ids
		 */
		final Set<String> allEntries = Collections.synchronizedSet(new HashSet<String>());
		
		ArrayList<List<String>> idLists = new ArrayList<List<String>>();
		ArrayList<String> currentIdList = new ArrayList<String>();
		idLists.add(currentIdList);
		for (String id : ids){
			currentIdList.add(id);
			
			if (currentIdList.size() >= idPerRequest){
				currentIdList = new ArrayList<String>();
				idLists.add(currentIdList);
			}
		}
		
		final AtomicBoolean exceptionCaught = new AtomicBoolean(false);
		ArrayList<Thread> threads = new ArrayList<Thread>();
		
		for (int i = 0; i < idLists.size(); i++){
			final List<String> idList = idLists.get(i);
			final int threadNumber = i + 1;
			
			Thread thread = new Thread(){
				@Override
				public void run() {
					try {
						fetchPSICQUIC(idList, allEntries, threadNumber);
					} catch (Exception e) {
						System.out.println(e.getMessage());
						exceptionCaught.set(true);
					}
				}
			};
			
			threads.add(thread);
		}

		HashSet<Thread> completed = new HashSet<Thread>();
		int active = 0;
		System.out.println("Number of ids per request: " + idPerRequest);
		System.out.println("Total number of request threads: " + threads.size());
		System.out.println("Active threads limit: " + activeThreadsLimit);
		while(true){
			if (cancelled){
				for (int i = 0; i < threads.size(); i++){
					threads.get(i).interrupt();
				}
				throw new Exception("Task Cancelled");
			}
			
			for (int i = 0; i < threads.size(); i++){
				Thread thread = threads.get(i);
				if (thread.getState() == State.TERMINATED && !completed.contains(thread)){
					completed.add(thread);
					active--;
				}
			}
			
			for (int i = 0; i< threads.size(); i++){
				Thread thread = threads.get(i);
				if (thread.getState() == State.NEW && active < activeThreadsLimit){
					thread.start();
					active++;
				}
			}
			
			
			if (exceptionCaught.get()){
				for (int i = 0; i < threads.size(); i++){
					threads.get(i).interrupt();
				}
				throw new Exception("Failed to fetch interaction data, possibly due to an Internet connection issue. Please retry.");
			}
			
			System.out.println("Completed: " + completed.size() + "/" + threads.size());
			taskMonitor.setProgress((double)completed.size() / threads.size());
			
			if (completed.size() == threads.size()){
				break;
			}else{
				Thread.sleep(1000);
			}
		}
		
		return allEntries;
	}

	
	private void fetchPSICQUIC(List<String> ids, Set<String> allEntries, int threadNumber) throws Exception{
		/**
		 * This fetches data from the PSICQUIC services. This function is called from within the
		 * multithreaded PSICQUIC fetch function.
		 */
		if (ids.isEmpty()) return;
		
		String databaseUrl = this.databaseUrl + "query/";
		
		String idList = ids.get(0);
		for (int i = 1; i < ids.size(); i++){
			idList += " " + ids.get(i);
		}
		
		String query = "identifier:(" + idList + ")";
		if (!species.isEmpty()){
			query += " AND taxidA:(" + species + ")";
			query += " AND taxidB:(" + species + ")";
		}
				
		
		String completeURL = databaseUrl + URLEncoder.encode(query, "utf-8").replace("+", "%20") + "?format=tab25&compressed=true";
		URL url = new URL(completeURL);
		int retryCount = 0;
		while(true){
			try{
				URLConnection urlConnection = url.openConnection();
				urlConnection.setRequestProperty("Accept-Encoding", "gzip");
				urlConnection.setConnectTimeout(30000);
				urlConnection.setReadTimeout(100000);
				
				InputStream inputStream;
				if (urlConnection.getContentEncoding() != null && urlConnection.getContentEncoding().equalsIgnoreCase("gzip")){
					inputStream = new GZIPInputStream(urlConnection.getInputStream());
				}else{
					inputStream = urlConnection.getInputStream();
				}
				
				BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));
				String line;
				while((line = in.readLine()) != null) {
					allEntries.add(line);
				}
				in.close();
				break;
			}catch (Exception e){
				if (retryCount < retryLimit){
					retryCount++;
					System.out.println("Exception in thread " + threadNumber + ". Retrying (" + retryCount + "/" + retryLimit + ")");
					System.out.println(e.getMessage());
				}else{
					System.out.println("Exception in thread " + threadNumber + ". Retry limit exceeded.");
					throw e;
				}
			}
		}
	}
	
	private static String extractValidId(String originalId){
		originalId = originalId.split("\\|", 2)[0];
		if (originalId.indexOf(":") != -1){
			return originalId.split(":", 2)[1].trim().toLowerCase();    //set limit because the id might contain colon
		}else{
			return originalId.trim().toLowerCase();
		}
	}
}
